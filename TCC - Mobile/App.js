import React, { Component  } from 'react';
import { Provider} from 'react-redux';
import {createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';


import Routes from './src/Routes';
import reducers from './src/reducers'


export default class App extends Component {

    componentWillMount(){
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyC68RwpihS4F-O69ZTt6Svzfr0ktB9NSRA",
            authDomain: "tcc-bar-menu.firebaseapp.com",
            databaseURL: "https://tcc-bar-menu.firebaseio.com",
            projectId: "tcc-bar-menu",
            storageBucket: "",
            messagingSenderId: "954341905838"
          };
          firebase.initializeApp(config);
    }

    render(){
        return (
            <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))} >
                <Routes />
            </Provider>
        );
    }
}
