import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import {
    MODIFICA_EMAIL, 
    MODIFICA_SENHA, 
    MODIFICA_NOME, 
    CADASTRA_USUARIO_SUCESSO,
    CADASTRA_USUARIO_ERRO,
    LOGIN_USUARIO_SUCESSO,
    LOGIN_USUARIO_ERRO,
    LOGIN_EM_ANDAMENTO,
    CADASTRO_EM_ANDAMENTO,
    MODIFICA_DATA_NASCIMENTO,
    MODIFICA_SEXO,
    MODIFICA_CIDADE,
    CADASTRAO_SUCESSO_PRIMEIRA_ETAPA,
    MODIFICA_PERGUNTA_UM,
    MODIFICA_PERGUNTA_DOIS,
    MODIFICA_PERGUNTA_TRES,
    LOGOUT_SISTEMA,
    IP_LOCALHOST
} from './types.js';

//Action creator é a função em si
export const modificaEmail = (texto) => {
    //Action em si é o retorno da função
    return {
        //toda Action necessita de um type
        type: MODIFICA_EMAIL,
        //o nome payload para a váriavel é apenas o valor sugerido pela documentação nesse caso pode ser qualquer valor
        payload: texto
    }
}

export const modificaPerguntaUm = (texto) => {
    return {
        type: MODIFICA_PERGUNTA_UM,
        payload: texto
    }
}
export const modificaPerguntaDois = (texto) => {
    return {
        type: MODIFICA_PERGUNTA_DOIS,
        payload: texto
    }
}
export const modificaPerguntaTres = (texto) => {
    return {
        type: MODIFICA_PERGUNTA_TRES,
        payload: texto
    }
}

export const modificaSenha = (texto) => {
    return {
        type: MODIFICA_SENHA,
        payload: texto
    }
}

export const modificaNome = (texto) => {
    return {
        type: MODIFICA_NOME,
        payload: texto
    }
}

export const modificaDataNascimento = (texto) => {
    return {
        type: MODIFICA_DATA_NASCIMENTO,
        payload: texto
    }
}

export const modificaSexo = (texto) => {
    return {
        type: MODIFICA_SEXO,
        payload: texto
    }
}

export const modificaCidade = (texto) => {
    return {
        type: MODIFICA_CIDADE,
        payload: texto
    }
}


export const cadastraUsuario = (dados) => {
  return dispatch => {
    dispatch( { type: CADASTRO_EM_ANDAMENTO} );
    firebase.auth().createUserWithEmailAndPassword(dados.email,dados.senha)
        .then(user => {
            fetch('http://'+IP_LOCALHOST+':8080/usuario', {  
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: dados.email,
                login: dados.email,
                idUsuarioFirebase: user.uid,
                nome: dados.nome,
                sexo: dados.sexo,
                dataNascimento: dados.dataNascimento,
                cidade: dados.cidade,
                senha: 'xiblau',
            })
            
            }).then((response) => response.json())
            .then(responseJson =>  cadastroUsuarioSucessoPrimeiraEtapa(responseJson ,dispatch))
            .catch(erro => cadastroUsuarioErro(erro,dispatch));
        }).catch(erro => cadastroUsuarioErro(erro,dispatch));
  }
    
}

const cadastroUsuarioSucessoPrimeiraEtapa = (idUser, dispatch) => {
    dispatch( {type: CADASTRAO_SUCESSO_PRIMEIRA_ETAPA, payload: idUser});
    Actions.CadastroPerguntasUsuario();
}

export const finalizaCadastroUsuario = (dados) => {
    return dispatch => {
        dispatch( { type: CADASTRO_EM_ANDAMENTO} );
            fetch('http://'+IP_LOCALHOST+':8080/usuario/'+dados.idUsuario, {  
            method: 'PUT',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                perguntaUm: dados.perguntaDoceSalgado,
                perguntaDois: dados.perguntaRestricao,
                perguntaTres: dados.perguntaTipoRestaurante
            })
            
            }).then((response) =>cadastroUsuarioSucesso(dispatch))
            .catch(erro => cadastroUsuarioErro(erro,dispatch));
    }
}


const cadastroUsuarioSucesso = (dispatch) => {
    dispatch( {type: CADASTRA_USUARIO_SUCESSO});
    Actions.formLogin();
}
const cadastroUsuarioErro = (erro, dispatch) => {
    dispatch( {type:CADASTRA_USUARIO_ERRO, payload: erro.message});
}

export const autenticarUsuario = ({email, senha}) => {
    return dispatch => {
        dispatch({ type: LOGIN_EM_ANDAMENTO });
        firebase.auth().signInWithEmailAndPassword(email,senha)
            .then(value =>  fetch('http://'+IP_LOCALHOST+':8080/usuario/byIdFirebase?idUsuarioFirebase='+value.uid))
            .then((response) => response.json())
            .then(value => loginUsuarioSucesso(value,dispatch))
            .catch(erro => loginUsuarioErro (erro, dispatch));
    }
}

const loginUsuarioSucesso = (value,dispatch) => {
    console.log(value)
    dispatch  (
        {
            type: LOGIN_USUARIO_SUCESSO,
            payload:value
        }
    );
    Actions.restaurantes();
}
const loginUsuarioErro = (erro, dispatch) => {
    console.log(erro);
    dispatch  (
        {
            type: LOGIN_USUARIO_ERRO,
            payload: erro.message
        }
    );
}

export const logoutSistema = () => {
    return dispatch => {
        dispatch({ type: LOGOUT_SISTEMA })
        firebase.auth().signOut()
        .then(Actions.formLogin())
    }
}