export const MODIFICA_EMAIL = 'modifica_email';
export const MODIFICA_SENHA = 'modifica_senha';
export const MODIFICA_NOME = 'modifica_nome';
export const MODIFICA_DATA_NASCIMENTO = 'modifica_data_nascimento';
export const MODIFICA_SEXO = 'modifica_sexo';
export const MODIFICA_CIDADE = 'modifica_cidade';
export const MODIFICA_PERGUNTA_UM = 'modifica_pergunta_um';
export const MODIFICA_PERGUNTA_DOIS = 'modifica_pergunta_dois';
export const MODIFICA_PERGUNTA_TRES = 'modifica_pergunta_tres';
export const CADASTRA_USUARIO_SUCESSO = 'cadastra_usuario_sucesso';
export const CADASTRA_USUARIO_ERRO = 'cadastra_usuario_erro';
export const LOGIN_USUARIO_SUCESSO = 'login_usuario_sucesso';
export const LOGIN_USUARIO_ERRO = 'login_usuario_erro';
export const LOGIN_EM_ANDAMENTO = 'login_em_andamento';
export const CADASTRO_EM_ANDAMENTO = 'cadastro_em_andamento';
export const LISTA_RESTAURANTES = 'lista_restaurantes';
export const LISTA_MENU = 'lista_menu';
export const LISTA_COMANDA = 'lista_comanda';
export const ENVIA_PEDIDO = 'envia_pedido';
export const SET_RESTAURANTE = 'set_restaurante';
export const MODIFICA_COMENTARIO_PEDIDO = 'modifica_comentario_pedido';
export const MODIFICA_QUANTIDADE = 'modifica_quantidade';
export const LIMPA_VARIAVEIS_PEDIDO = 'limpa_variaveis_pedido';
export const ABRE_TELA_PEDIDO = 'abre_tela_pedido';
export const CADASTRAO_SUCESSO_PRIMEIRA_ETAPA = 'cadastra_usuario_sucesso_primeira_etapa';
export const LOGOUT_SISTEMA = 'logout_sistema'





export const IP_LOCALHOST = '192.168.0.16';