import {Actions } from 'react-native-router-flux'; 
import firebase from 'firebase';
import _ from 'lodash';
import { LISTA_RESTAURANTES, 
    LISTA_MENU,
    LISTA_COMANDA,
    ENVIA_PEDIDO,
    SET_RESTAURANTE,
    MODIFICA_COMENTARIO_PEDIDO,
    MODIFICA_QUANTIDADE,
    LIMPA_VARIAVEIS_PEDIDO,
    ABRE_TELA_PEDIDO,
    IP_LOCALHOST
} from './types.js';


export const fetchDataRestaurante = (filtro,pagina) => {
    return (dispatch ) => {
        fetch("http://"+IP_LOCALHOST+":8080/estabelecimento")
        .then((response) => response.json())
        .then((responseJson) => {
            dispatch({  
                type: LISTA_RESTAURANTES,
                payload: responseJson 
            })
        });
      
    }
  };


export const fetchDataMenu = (idRestaurante) => {
    return (dispatch ) => {
        fetch("http://"+IP_LOCALHOST+":8080/itemMenu/"+idRestaurante)
        .then((response) => response.json())
        .then((responseJson) => {
            responseJson.forEach(function(item) {
                _.assign(item,{color:'#FFF'})
            });
            var itemRecomendado = {};
            fetch("http://"+IP_LOCALHOST+":8080/itemMenu/3")
            .then((resposta) => resposta.json())
            .then((respostaJson) => {
                respostaJson.forEach(function(item) {
                    _.assign(item,{color:'#FFD700',isItemRecomendado:true});
                    responseJson.unshift(item);
                });
                return responseJson;
            }).then((responseJson) => {
           
                dispatch({  
                    type: LISTA_MENU,
                    payload: responseJson 
                })
            })
            .catch(erro => console.log(erro.code));
        }).catch(erro => console.log(erro.code));
    }
};



export const fetchDataResumoComanda = (idComanda) => {
    return (dispatch ) => {
        getComanda(dispatch,idComanda)
    }
       
};

export const getComanda= (dispatch, idComanda) =>{
    fetch("http://"+IP_LOCALHOST+":8080/pedido/pedidoAgrupado?idComanda="+idComanda)
    .then((response) => response.json())
    .then((responseJson) => {
        dispatch({  
            type: LISTA_COMANDA,
            payload: responseJson 
        })
    }).catch(erro => console.log(erro));

}



export const setRestauranteEscolhido = (restaurante) => {
    return({
        type: SET_RESTAURANTE,
        payload: restaurante
    });
}

export const modificaComentarioPedido = (comentario) =>{
    return ({
        type: MODIFICA_COMENTARIO_PEDIDO,
        payload: comentario
    });
}

export const modificaQuantidadePedido = (qtd,valorPedido) =>{
    //const formatoMonetario = { minimumFractionDigits: 2 , style: 'currency', currency: 'BRL' }
    //const valorTotal = (qtd * valorPedido).toLocaleString('pt-BR', formatoMonetario);
    const valorTotal = parseFloat((qtd * valorPedido)).toFixed(2);
    return ({
        type: MODIFICA_QUANTIDADE,
        payload: {qtd,valorTotal}
    });
}

export const limpaCamposPedido = () => {
    return ({
        type: LIMPA_VARIAVEIS_PEDIDO
    });
}

export const gerenciaComanda = (informacoes)  => {
    return dispatch => {
            var valorTotalComanda = parseFloat(informacoes.valorAtualComanda) + parseFloat(informacoes.valorTotal);
            if(informacoes.idComanda){
                fetch('http://'+IP_LOCALHOST+':8080/comanda/AtualizaValorTotal?idComanda='+informacoes.idComanda, {  
                    method: 'PUT',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        valorTotal: valorTotalComanda,
                    })                   
                }).then( enviaPedido(dispatch,informacoes,informacoes.idComanda))
            }else{
                fetch('http://'+IP_LOCALHOST+':8080/statusComanda/byEstabelecimento?nome=Aberto&idEstabelecimento='+informacoes.restaurante.idEstabelecimento)
                .then((responseStatusComanda) => responseStatusComanda.json())
                .then(idStatusComanda => {
                    fetch('http://'+IP_LOCALHOST+':8080/comanda', {  
                    method: 'POST',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        idEstabelecimento: informacoes.restaurante.idEstabelecimento,
                        idUsuario: informacoes.idUsuario,
                        statusComanda: idStatusComanda,
                        valorTotal: valorTotalComanda,
                    })

                    
                    }).then((response) => response.json())
                    .then(idComanda => enviaPedido(dispatch,informacoes,idComanda))
                }); 
            }
                
      }
};


const enviaPedido = (dispatch,informacoes , idComanda) =>{
    var valorTotalComanda = parseFloat(informacoes.valorAtualComanda) + parseFloat(informacoes.valorTotal);
    return(
        fetch('http://'+IP_LOCALHOST+':8080/statusPedido/byEstabelecimento?nome=Aberto&idEstabelecimento='+informacoes.restaurante.idEstabelecimento)
        .then((responseStatusPedido) => responseStatusPedido.json())
        .then(idStatusPedido =>
            fetch('http://'+IP_LOCALHOST+':8080/pedido', {  
                method: 'POST',
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    idEstabelecimento: informacoes.restaurante.idEstabelecimento,
                    idComanda: idComanda,
                    idItemMenu: informacoes.pedido.id,
                    idStatusPedido: idStatusPedido,
                    observacaoPedido: informacoes.comentarioPedido,
                    quantidade: informacoes.quantidade
                })
                
            }).then((response) => response.json())
            .then(responseJson =>  {
                getComanda(dispatch,idComanda)
                dispatch({  
                    type: ENVIA_PEDIDO,
                    payload: {idComanda:idComanda, valorAtualComanda:valorTotalComanda} 
                })
            })
        )
    );
}

export const abreTelaPedido = (itemMenuEscolhido) => {
    Actions.pedido();
    return({
        type: ABRE_TELA_PEDIDO,
        payload:itemMenuEscolhido
    })      
    
}