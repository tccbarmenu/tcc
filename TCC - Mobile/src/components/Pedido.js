import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { TabViewAnimated, SceneMap } from 'react-native-tab-view';
import TabBarMenu from './TabBarMenu.js';
import ComponentesPedido from './ComponentesPedido.js';
import DescricaoPedido from './DescricaoPedido.js';

export default class Pedido extends Component {
  state = {
    index: 0,
    routes: [
      { key: '1', title: 'Descrição' },
      { key: '2', title: 'Componentes' },
    ],
  };

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = props => <TabBarMenu {...props} isTabViewAnimated={true} title={this.props.title} />;

  _renderScene = SceneMap({
    '1': DescricaoPedido,
    '2': ComponentesPedido,
  });

  render() {
    return (
      <TabViewAnimated
        style={styles.container}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        onIndexChange={this._handleIndexChange}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});