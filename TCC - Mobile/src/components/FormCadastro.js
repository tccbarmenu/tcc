import React , { Component } from 'react';
import {View, TextInput, Button, StyleSheet, Image, Text, ActivityIndicator, Picker} from 'react-native';
import { connect } from 'react-redux';
import { modificaEmail, modificaSenha, modificaNome,modificaDataNascimento, modificaSexo, modificaCidade, cadastraUsuario } from '../actions/AutenticacaoActions';
import DatePicker from 'react-native-datepicker'

class formCadastro extends Component {

    _cadastraUsuario(){
        this.props.cadastraUsuario(this.props);
    }

    renderBtnCadastrar(){
        if(this.props.loadingCadastro){
            return (
                <ActivityIndicator size='large' color='white'/>
            );
        }
        return(
            <Button title='Cadastrar' color='#e20d0d' onPress={() => this._cadastraUsuario()} />
        );
    }

    render(){
        return(
            <Image style={{ flex:1, width: null}} source={ require('../imgs/Fundo2.png') } >
                <View style={styles.telaPrincipal} >
                    <View style={styles.telaFormCadastro} >
                        <TextInput 
                            value={ this.props.nome} 
                            placeholder='Nome'
                            placeholderTextColor='#fff' 
                            style={styles.textInputCadastro}  
                            onChangeText={ texto => this.props.modificaNome(texto) }
                            underlineColorAndroid='#fff'
                         />
                        <TextInput 
                            value={ this.props.email} 
                            placeholder='E-mail' 
                            placeholderTextColor='#fff' 
                            style={styles.textInputCadastro} 
                            onChangeText={ texto => this.props.modificaEmail(texto) } 
                            underlineColorAndroid='#fff'
                        />
                        <TextInput 
                            secureTextEntry 
                            value={ this.props.senha} 
                            placeholder='Senha' 
                            placeholderTextColor='#fff' 
                            style={styles.textInputCadastro}  
                            onChangeText={ texto => this.props.modificaSenha(texto) } 
                            underlineColorAndroid='#fff'
                         />
                         <TextInput 
                            value={ this.props.cidade} 
                            placeholder='Cidade' 
                            placeholderTextColor='#fff' 
                            style={styles.textInputCadastro}  
                            onChangeText={ texto => this.props.modificaCidade(texto) } 
                            underlineColorAndroid='#fff'
                         />
                         <View style={{flexDirection:'row',justifyContent:'space-between', margin: 5}}>
                            <View style={{ flex:6, margin: 5}}>
                                <DatePicker
                                    style={{width: 150, height:45}}
                                    date={this.props.dataNascimento}
                                    mode="date"
                                    placeholder="Data de Nascimento"
                                    format="DD-MM-YYYY"
                                    minDate="01-01-1900"
                                    maxDate="01-01-2200"
                                    confirmBtnText="Confirma"
                                    cancelBtnText="Cancela"
                                    customStyles={{
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0,
                                        borderWidth:1
                                    },
                                    dateInput: {
                                        marginLeft: 36,
                                       
                                    },
                                    dateText:{
                                        color:'white'
                                    }
                                    }}
                                    onDateChange={(date) => {this.props.modificaDataNascimento(date)}}
                                />
                            </View>
                            <View style={{ borderColor:'#fff',borderWidth:1, flex:5, margin: 5 , height: 40}}>
                                <Picker
                                    style={{color:'#fff'}}
                                    selectedValue={this.props.sexo}
                                    onValueChange={(itemValue, itemIndex) =>  {this.props.modificaSexo(itemValue)}}>
                                    <Picker.Item label="Feminino" value="F" />
                                    <Picker.Item label="Masculino" value="M" />
                                </Picker>
                            </View>
                        </View>
                        <Text style={styles.txtErro} > {this.props.erroCadastro}</Text>
                    </View>
                    <View style={styles.telaFormButton} >
                        { this.renderBtnCadastrar() }
                    </View>
                </View>
            </Image>
        );
    }
}

const mapStateToProps = state => (
    {
        email: state.AutenticacaoReducer.email,
        senha: state.AutenticacaoReducer.senha,
        nome: state.AutenticacaoReducer.nome,
        dataNascimento: state.AutenticacaoReducer.dataNascimento,
        cidade:state.AutenticacaoReducer.cidade,
        sexo:state.AutenticacaoReducer.sexo,
        erroCadastro: state.AutenticacaoReducer.erroCadastro,
        loadingCadastro: state.AutenticacaoReducer.loadingCadastro
    }
);

const styles = StyleSheet.create({
    telaPrincipal: {
        flex: 1,
    },
    telaFormCadastro: {
        flex:6,
        justifyContent: 'center',
        padding: 10
    },
    textInputCadastro: {
        fontSize: 20,
        height: 45,
        color:'#fff'
    },
    telaFormButton: {
        flex: 1,
        justifyContent:'flex-end'
    },
    txtErro: {
        color: '#ff0000',
        fontSize: 18
    }

});

export default connect(mapStateToProps, { modificaEmail, modificaSenha, modificaNome, modificaDataNascimento, modificaSexo, modificaCidade, cadastraUsuario })(formCadastro);
