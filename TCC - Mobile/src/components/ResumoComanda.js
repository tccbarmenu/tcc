import React, {Component} from 'react';
import { FlatList, StyleSheet, Text, View,TextInput,Image,TouchableHighlight,Button  } from "react-native";
import Search from 'react-native-search-box';
import { Actions} from 'react-native-router-flux';
import { connect }from 'react-redux';

import { fetchDataResumoComanda } from '../actions/AppActions.js'

class ResumoComanda extends Component {
    componentWillMount() {
        this.props.fetchDataResumoComanda(this.props.idComanda);
    }


    itemComanda(item){
        const valorTotalItem = parseFloat(item.valorTotalPedidoAgrupado).toFixed(2);
        return(
                <View style={{flexDirection: 'row', justifyContent:'space-between', flex: 1, paddingVertical:5, paddingHorizontal: 20}}>
                        <Text style={{flex: 1 , fontSize: 16, color: 'black'}}>{item.quantidade}</Text>
                        <Text style={{flex: 3  , fontSize: 16, color: 'black', alignItems: 'stretch',}} >{item.nomePedido}</Text>
                        <Text style={{flex: 1  ,fontSize: 16,paddingLeft: 5, color: 'black'}}>{valorTotalItem}</Text>
                </View>

        );
    }

    
    render(){
        return(
            <View style={styles.tela} >
                <View style={{flex: 2}}>
                    <View style={{flexDirection: 'row', flex: 1, borderWidth:1, borderRadius: 8, margin:5, paddingVertical: 5, paddingHorizontal: 10 }}>
                        <View style={{ flex: 1 , justifyContent:'center'}}>
                            <Text style={{fontWeight: 'bold', fontSize: 16, color: 'black'}} >Qtd</Text>
                        </View>
                        <View style={{ flex: 3, justifyContent:'center' }}>
                            <Text style={{fontWeight: 'bold', fontSize: 16, color: 'black' }} >Pedido</Text>
                        </View>
                        <View  style={{ flex: 1, justifyContent:'center' }}>
                            <Text style={{fontWeight: 'bold', fontSize: 16, color: 'black' }} >Valor</Text>
                        </View>
                    </View>
                </View>
                <View style={{flex: 10, borderWidth:1, borderRadius: 8, margin:5, padding: 5 }}>
                    <View >
                        <FlatList
                            data={this.props.itens_comanda}
                            keyExtractor={(x, i) => i}
                            renderItem={({ item }) => this.itemComanda(item)}
                        
                        />
                    </View>
                </View>
                <View style={{flex: 2, justifyContent:'center'}}>
                    <View style={{flexDirection: 'row', flex: 1, borderWidth:1, borderRadius: 8, margin:5, paddingVertical: 5, paddingHorizontal: 10 }}>
                        <View style={{ flex: 1, justifyContent:'center'}}>
                            <Text style={{fontWeight: 'bold', fontSize: 24, color: '#3eb308'}} >Total</Text>
                        </View>
                        <View style={{ flex: 1, justifyContent:'center' }}>
                            <Text style={{fontWeight: 'bold', fontSize: 24, color: 'black', textAlign:'right', }} >{parseFloat(this.props.valorAtualComanda).toFixed(2)}</Text>
                        </View>
                    </View>
                </View>
                <View style={{flex: 1.3 , padding: 5,   justifyContent: 'flex-end'}}>
                    <Button title='Encerrar' color='#e20d0d' style={{  borderRadius:  8 }}  onPress={ () => Actions.avaliacao()} /> 
                </View>
             </View>
        );
    }

}


const styles = StyleSheet.create({
    tela: {
        flex: 1,
        backgroundColor: 'white'
    },
   
});

mapStateToProps = state => {
    return({
        itens_comanda: state.ListaComandaReducer.itens_comanda,
        idUsuario : state.AutenticacaoReducer.idUsuario,
        valorAtualComanda : state.AppReducer.valorAtualComanda,
        idComanda: state.AppReducer.idComanda
    })
}

export default connect(mapStateToProps, { fetchDataResumoComanda })(ResumoComanda);