import React, {Component} from 'react';
import { FlatList, StyleSheet, Text, View,TextInput,Image,TouchableHighlight  } from "react-native";
import Search from 'react-native-search-box';
import { Actions} from 'react-native-router-flux';
import { connect }from 'react-redux';
import { IP_LOCALHOST} from '../actions/types.js';

import { fetchDataMenu,limpaCamposPedido,abreTelaPedido } from '../actions/AppActions.js'

class Menu extends Component {
    componentWillMount() {
        this.props.fetchDataMenu(this.props.restauranteEscolhido.idEstabelecimento);
    }


    _selecionaItemMenu(item){
        this.props.limpaCamposPedido();
        this.props.abreTelaPedido(item);
    }

    itemMenu(item){
        var valor = parseFloat(item.preco).toFixed(2);
        var imagem = item.imagem;
        var corItem = item.color;
        if(imagem == null){
            imagem = 'pratox.jpg';
        }
        imagem = 'http://'+IP_LOCALHOST+'/upload/'+imagem;
        return(
            <TouchableHighlight onPress={ () =>  this._selecionaItemMenu(item) }>
                <View style={styles.item}>
                    <View style={[styles.viewImagem,{backgroundColor:corItem}]}>
                        <Image style={styles.imagemCatalogo} source={{ uri: imagem }}/>
                    </View>
                    <View style={{flex:3}}>
                        <View style={[styles.descricaoItem,{backgroundColor:corItem}]}>
                            <View style={{flexDirection:'row', justifyContent: 'space-between'}}>
                                <Text style={styles.title}>{item.titulo}</Text>
                                {this.getTextToRecommendation(item)}
                            </View>
                            <Text style={styles.texto}>{item.descricao}</Text>
                        </View>
                        <View style={{flex:1,backgroundColor: corItem,alignItems: 'stretch',flexDirection: 'row', padding: 5, justifyContent: 'space-between'}}>
                                <Text style={styles.texto}>Serve: {item.serveQtdPessoas} </Text>
                                <Text style={styles.texto}>Valor: ${valor}  </Text>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        );
    }
    
    getTextToRecommendation(item){
        if(item.isItemRecomendado){
           return (
                    <Text style={{fontSize:12,fontWeight:'bold', color:'red'}} >Recomendado</Text>
           ) 
        }
    }

    
    render(){
        return(
            <View style={styles.tela} >
                <View>
                    <FlatList
                        data={this.props.info_menu}
                        keyExtractor={(x, i) => i}
                        renderItem={({ item }) => this.itemMenu(item)}
                     
                    />  
                </View>
             </View>
        );
    }

}

const styles = StyleSheet.create({
    tela: {
        flex: 1,
        backgroundColor: '#aaaaaa'
    },
    item: {
        flexDirection: 'row',
        padding: 5,
        backgroundColor: '#aaaaaa' ,
        height: 120,
    },
    viewImagem: {
        flex: 1,
        backgroundColor: 'white',
        padding: 5
    },
    imagemCatalogo: {
        height: 80,
        width: 80,
        borderRadius:  30
    },
    descricaoItem: {
        flex: 2,
        backgroundColor: 'white',
        alignItems: 'stretch',
        padding: 5
    },
    title: {
        fontSize: 18,
        color: '#000000',
        fontWeight: 'bold',
        marginBottom: 5
    },
    text: {
        fontSize: 8
    }
    
});

mapStateToProps = state => {
    return({
        page_menu : state.ListaMenuReducer.page_menu,
        info_menu: state.ListaMenuReducer.info_menu,
        restauranteEscolhido: state.ListaMenuReducer.restauranteEscolhido,

    })
}

export default connect(mapStateToProps, { fetchDataMenu,limpaCamposPedido,abreTelaPedido })(Menu);