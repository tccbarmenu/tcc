import React, {Component} from 'react';
import {View, Text, StatusBar, Image, TouchableHighlight} from 'react-native';
import {TabBar} from 'react-native-tab-view';
import {Actions } from 'react-native-router-flux';
import {connect} from 'react-redux';
import firebase from 'firebase'
import {logoutSistema} from '../actions/AutenticacaoActions';
class TabBarMenu extends Component {


    renderTabBar(){
        if(this.props.isTabViewAnimated){
            return (
                <TabBar{...this.props} style={{ backgroundColor:'#e20d0d', elevation:0 }}/>
            );
        }
    }


    render(){
        return(
            <View style={{ backgroundColor:'#e20d0d', elevation:4 }} >
            
                <StatusBar  backgroundColor='#e20d0d'/>
        
                <View style={{ flexDirection:'row', justifyContent: 'space-between' }}>
                    <View style={{ height:50, justifyContent: 'center' }}>
                        <Text style={{ color:'#fff', fontWeight:'bold', fontSize:20 , marginLeft:20 }}>{this.props.title}</Text>
                    </View>
                    <View style={{ flexDirection:'row', marginRight: 20 }}>
                        <View style={{ justifyContent:'center' }}>
                            <TouchableHighlight onPress={() => this.props.logoutSistema()} underlayColor='#114D44'>
                                <Text style={{ fontSize:20, color:'#fff', fontWeight:'bold' }} >Sair</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
                { this.renderTabBar() }
            </View>
        );
    }
}


export default connect(null , {logoutSistema } )(TabBarMenu);