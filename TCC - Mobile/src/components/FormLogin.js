import React, { Component } from 'react';
import {View, TextInput, Text, Button, StyleSheet, TouchableHighlight , Image, ActivityIndicator } from 'react-native';
import { Actions} from 'react-native-router-flux';

import { connect }from 'react-redux';
import { modificaEmail, modificaSenha, autenticarUsuario } from '../actions/AutenticacaoActions';

class formLogin extends Component {

    _autenticarUsuario(){
        const { email, senha } = this.props;
        this.props.autenticarUsuario({email,senha});
    }

    renderBtnAcessar(){
        if(false){
            return (
                <ActivityIndicator size='large' color='white'/>
            );
        }
        return (
            <Button 
                title='Acessar' 
                color='#e20d0d' 
                onPress={ () => this._autenticarUsuario()} 
            /> 
        );
    }

    render(){
        return(
        <Image style={{ flex:1, width: null}} source={ require('../imgs/Fundo.png') } >
            <View style={styles.telaPrincipal} >
                <View style={styles.formulario}>
                     <TextInput 
                            style={styles.textInputLogin} 
                            value={this.props.email} 
                            placeholder='E-mail' placeholderTextColor='#fff' 
                            onChangeText={texto => this.props.modificaEmail(texto) } 
                            selectionColor= '#fff'
                            underlineColorAndroid='#fff'
                        />
                    <TextInput 
                        secureTextEntry 
                        value={this.props.senha} 
                        style={styles.textInputLogin} 
                        placeholder='Senha' 
                        placeholderTextColor='#fff' 
                        onChangeText={texto => this.props.modificaSenha(texto) } 
                        underlineColorAndroid='#fff'
                    /> 
                    <Text style={styles.erroLogin} > {this.props.erroLogin} </Text>
                    <TouchableHighlight onPress={ () => Actions.formCadastro() } >  
                        <Text style={styles.textoCadastreSe} >Ainda não tem cadastro? Cadastre-se </Text>
                    </TouchableHighlight>
                </View>
                <View style={styles.botao}>
                    { this.renderBtnAcessar() }
                </View>
            </View>
        </Image>
        );
    }
}


const mapStateToProps = state => (
    {
        email: state.AutenticacaoReducer.email ,
        senha: state.AutenticacaoReducer.senha,
        erroLogin: state.AutenticacaoReducer.erroLogin,
        loadingLogin: state.AutenticacaoReducer.loadingLogin
    }
);

const styles = StyleSheet.create({
    telaPrincipal: {
        flex:1,
        justifyContent: 'center'
    },
    titulo: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textoTitulo: {
        fontSize: 25,
        color: '#fff'
    },
    formulario: {
        flex:2,
        justifyContent:'center',
        padding: 10,
    },
    textInputLogin: {
        fontSize: 20,
        height: 45,
        color:'#fff',
        
    },
    textoCadastreSe: {
        fontSize:20,
        color: '#fff'   
    },
    botao: {
        justifyContent:'flex-end'
    },
    erroLogin: {
        color: '#ff0000',
        fontSize: 18
    }

    
});
export default connect(mapStateToProps, { modificaEmail: modificaEmail, modificaSenha: modificaSenha, autenticarUsuario: autenticarUsuario })(formLogin);