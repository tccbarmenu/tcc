import React, {Component} from 'react';
import {  Text, View} from "react-native";
import { Actions} from 'react-native-router-flux';
import { connect }from 'react-redux';
import CustomMultiPicker from "react-native-multiple-select-list";
import _ from 'lodash';


class ComponentesPedido extends Component {

    _createListIngredientes(){
        var listItens = {};
        var model = {
            id:null,
            nome:null
         };
         _(this.props.pedido.ingredientes).forEach(function(ingrediente){
            var result = _.pick(ingrediente, _.keys(model));
            listItens[result.id] = result.nome;
         });
        return listItens;
    }
    
    render(){
       
    const ingredientes = this._createListIngredientes();
        return(
            <View style={{backgroundColor: '#aaaaaa', flex: 1}}>
               <CustomMultiPicker
                    options={ingredientes}
                    search={true} // should show search bar? 
                    multiple={true} // 
                    placeholder={"Search"}
                    placeholderTextColor={'#757575'}
                    returnValue={"label"} // label or value 
                    callback={(res)=>{ console.log() }} // callback, array of selected items 
                    rowBackgroundColor={"white"}
                    rowHeight={40}
                    rowRadius={5}
                    iconColor={"#3eb308"}
                    iconSize={30}
                    allSelected={true}
                    selectedIconName={"ios-checkmark-circle-outline"}
                    unselectedIconName={"ios-radio-button-off-outline"}
                    selected={[1,2]} // list of options which are selected by default 
                />
             </View>
        );
    }

}

  mapStateToProps = state => {
    return({
        pedido: state.ListaMenuReducer.ultimoItemMenuEscolhido
    })
}


export default connect(mapStateToProps, {})(ComponentesPedido);