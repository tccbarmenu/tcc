import React, {Component} from 'react';
import { FlatList, StyleSheet, Text, View,TextInput,Image,TouchableHighlight  } from "react-native";
import Search from 'react-native-search-box';
import { Actions} from 'react-native-router-flux';
import { connect }from 'react-redux';


import TabBarMenu from './TabBarMenu.js';
import { fetchDataRestaurante, setRestauranteEscolhido } from '../actions/AppActions.js'

class Restaurantes extends Component {



    onSearch(texto){
        alert(this.props.title);
    }

    componentWillMount() {
        this.props.fetchDataRestaurante(this.props.filtro_restaurante, this.props.page_restaurante);
    }

    _defineRestauranteEscolhido(restaurante){
        this.props.setRestauranteEscolhido(restaurante);
        Actions.comanda({title:restaurante.nomeEstabelecimento});
    }

    componentRestaurante(restaurante){
        return(
            <TouchableHighlight onPress={ () =>  this._defineRestauranteEscolhido(restaurante)  }>
                <View style={styles.item}>
                    <View style={styles.viewImagem}>
                        <Image style={styles.imagemCatalogo} source={ require('../imgs/logo-generica-restaurante.jpg')} />
                    </View>
                    <View style={{flex:3}}>
                        <View style={styles.descricaoItem}>
                            <Text style={styles.title}>{restaurante.nomeEstabelecimento}</Text>
                            <Text style={styles.texto}>{restaurante.descricaoEstabelecimento}</Text>
                        </View>
                        <View style={{flex:1,backgroundColor: 'white',alignItems: 'stretch',flexDirection: 'row', padding: 5, justifyContent: 'space-between'}}>
                            <Text style={{flex:1}}>Cidade: {restaurante.cidade} </Text>
                            <Text style={{flex:1}}>Endereço: {restaurante.endereco} </Text>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>

        );
    }

    
    render(){
        return(
            <View style={styles.tela} >
                 <View >
                    <TabBarMenu {...this.props}/>
                    <Search
                        ref="search_box"
                        onSearch={texto => this.onSearch(texto)}
                        backgroundColor='#e20d0d'
                    />
                </View>
                <View>
                    <FlatList
                        data={this.props.info_restaurante}
                        keyExtractor={(x, i) => i}
                        renderItem={({ item }) => this.componentRestaurante(item)}
                     
                    />
                </View>
             </View>
        );
    }

}


const styles = StyleSheet.create({
    tela: {
        flex: 1
    },
    item: {
        flexDirection: 'row',
        padding: 5,
        backgroundColor: '#aaaaaa',
        flex:1 
    },
    viewImagem: {
        flex: 1,
        backgroundColor: 'white',
        padding: 5
    },
    imagemCatalogo: {
        height: 100,
        width: 100,
        borderRadius:  30
    },
    descricaoItem: {
        flex: 2,
        backgroundColor: 'white',
        alignItems: 'stretch',
        padding: 5
    },
    title: {
        fontSize: 18,
        color: '#000000',
        fontWeight: 'bold',
        marginBottom: 5
    },
    text: {
        fontSize: 8
    }
    
});

mapStateToProps = state => {
    return({
        filtro_restaurante : state.ListaRestauranteReducer.filtro_restaurante,
        page_restaurante : state.ListaRestauranteReducer.page_restaurante,
        info_restaurante : state.ListaRestauranteReducer.info_restaurante,
        isTabViewAnimated: state.AutenticacaoReducer.isTabViewAnimated
    })
}

export default connect(mapStateToProps, { fetchDataRestaurante, setRestauranteEscolhido })(Restaurantes);