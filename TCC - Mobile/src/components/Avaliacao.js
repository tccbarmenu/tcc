import React, {Component} from 'react';
import { FlatList, StyleSheet, Text, View,TextInput,Image,TouchableHighlight,Button  } from "react-native";
import Search from 'react-native-search-box';
import { Actions} from 'react-native-router-flux';
import { connect }from 'react-redux';
import Stars from 'react-native-stars-rating';


import { fetchDataResumoComanda } from '../actions/AppActions.js'

class Avaliacao extends Component {



    itemAvaliacao(item){
        return(
                <View style={{flexDirection: 'row', justifyContent:'space-between', flex: 1, paddingVertical:5, paddingHorizontal: 20}}>
                        <Text style={{flex: 1  , fontSize: 16, color: 'black', alignItems: 'stretch',}} >{item.nomePedido}</Text>
                        <View style={{flex: 1}}>
                            <Stars
                                isActive={true}
                                rateMax={5}
                                isHalfStarEnabled={false}
                                onStarPress={(rating) => console.log(rating)}
                                rate={1}
                                size={30}
                                color={'#f0d817'}
                            />
                        </View>
                </View>

        );
    }

    
    render(){
        return(
            <View style={styles.tela} >
                <View style={{flex: 2, paddingTop: 60,  paddingLeft: 5,  paddingRight: 5, paddingBottom: 5, flexDirection: 'row'}}>
                    <Image style={styles.imagemCatalogo} source={ require('../imgs/logo-generica-restaurante.jpg')} />
                    <View style={{flex:2}}>
                        <View style={{flex:1}}>
                            <Text style={styles.titulo} >{this.props.restauranteEscolhido.titulo}</Text>
                        </View>
                        <View style={{flex:1,justifyContent: 'center', alignItems:'center'}}>
                            <Stars
                                    isActive={true}
                                    rateMax={5}
                                    isHalfStarEnabled={false}
                                    onStarPress={(rating) => console.log(rating)}
                                    rate={1}
                                    size={30}
                                    color={'#f0d817'}
                            />
                        </View>
                    </View>
                </View>
                <View style={{flex: 7, borderWidth:1, borderRadius: 8, margin:5, padding: 5 }}>
                    <View >
                        <FlatList
                            data={this.props.itens_comanda}
                            keyExtractor={(x, i) => i}
                            renderItem={({ item }) => this.itemAvaliacao(item)}
                        
                        />
                    </View>
                </View>
                <View style={{flex: 1 , flexDirection:'row'}}>
                    <View style={{flex: 1 , paddingHorizontal: 5}}>
                        <Button title='Cartão' color='#3eb308' style={{  borderRadius:  8 , flex: 1, }}  onPress={ () => this._finalizaComanda ()} /> 
                    </View>
                    <View style={{flex: 1, paddingHorizontal: 5 }}>
                        <Button title='Dinheiro' color='#f0d817' style={{  borderRadius:  8, flex: 1 }}  onPress={ () => this._finalizaComanda()} /> 
                    </View>
                </View>
             </View>
        );
    }


    _finalizaComanda(){

        Actions.restaurantes();
    }

}


const styles = StyleSheet.create({
    tela: {
        flex: 1,
        backgroundColor: 'white'
    },
    imagemCatalogo: {
        flex:1,
        height: 100,
        width: 100,
        borderRadius:  30
    },
    titulo:{
        fontSize: 25,
        fontWeight: 'bold',
        color: 'black',
        textAlign: 'center'
    }
   
});

mapStateToProps = state => {
    console.log(state);
    return({
        itens_comanda: state.ListaComandaReducer.itens_comanda,
        restauranteEscolhido: state.ListaMenuReducer.restauranteEscolhido
    })
}

export default connect(mapStateToProps, { fetchDataResumoComanda })(Avaliacao);