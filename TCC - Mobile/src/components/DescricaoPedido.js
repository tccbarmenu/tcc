import React, {Component} from 'react';
import { StyleSheet, Text, View,TextInput,Image,TouchableHighlight,QuantitySelector, Button,ScrollView  } from "react-native";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Actions} from 'react-native-router-flux';
import { connect }from 'react-redux';
import  Quantity  from 'react-quantity-textinput';
import { IP_LOCALHOST} from '../actions/types.js';

import { modificaComentarioPedido, modificaQuantidadePedido, gerenciaComanda } from '../actions/AppActions.js'

class DescricaoPedido extends Component {

    componentWillMount(){
        this.props.modificaQuantidadePedido(this.props.quantidade,  this.props.pedido.preco)
    }
   
    render(){
        const valorPedido = parseFloat(this.props.pedido.preco).toFixed(2);
        var imagem = this.props.pedido.imagem;
        if(imagem == null){
            imagem = 'pratox.jpg';
        }
        imagem = 'http://'+IP_LOCALHOST+'/upload/'+imagem;
        return(
            <KeyboardAwareScrollView style={styles.tela}  contentContainerStyle={{flexGrow: 1}} >
                 <View style={styles.cabecalho}>
                    <Image style={styles.imagemCatalogo} source={{ uri: imagem }} />
                    <View style={styles.tituloView}>
                        <Text style={styles.titulo}>{this.props.pedido.titulo}</Text>
                    </View>
                </View>
                <View style={styles.descricaoView}> 
                     <Text style={styles.texto}>{this.props.pedido.descricao}</Text>
                </View>
                <View style={{flexDirection: 'row', justifyContent:'space-between', flex : 1}}>
                    <View  style={{flexDirection: 'row', }} >
                        <Text style={[styles.texto,{fontWeight: 'bold', paddingRight: 5}]}>Serve:</Text>
                        <Text style={styles.texto} >{this.props.pedido.serveQtdPessoas} Pessoas</Text>
                    </View>
                    <View style={{flexDirection: 'row', }}>
                        <Text style={[styles.texto,{fontWeight: 'bold', paddingRight: 5}]} >Valor:</Text>
                        <Text style={styles.texto}>{valorPedido}</Text>
                    </View>
                </View>
                <View style={styles.quantidadeView}>
                    <Text style={styles.quantidadeText}>Quantidade: </Text>
                    <Quantity
                        style={{flex: 1,}}
                        styleTextInput={{backgroundColor:'#ffffff'}}
                        styleButton={{backgroundColor:'#000000'}}
                        styleImage={{width:12, height:12}}
                        editable={false}
                        initialValue={this.props.quantidade}
                        min={1}
                        onChangeText={ qtd => this.props.modificaQuantidadePedido(qtd, valorPedido)}
                        max={100} 
                    />
                </View>
                <View style={styles.comentarioView}>
                    <TextInput 
                        placeholder='Comentários' 
                        placeholderTextColor='#66737C'
                        style={styles.textInputComentarios} 
                        onChangeText={ texto => this.props.modificaComentarioPedido(texto) } 
                        multiline = {true}
                        numberOfLines = {4}
                        underlineColorAndroid='black'
                        value={this.props.comentarioPedido}
                    />
                </View>
                <View style={styles.quantidadeView}>
                    <Text style={styles.quantidadeText}>Total: </Text>
                    <Text style={styles.texto}>{this.props.valorTotal}</Text>
                </View>
                <View style={styles.buttonView}>
                    <Button title='Enviar Pedido' color='#3eb308' style={styles.button}  onPress={ () => this._enviaPedido() } /> 
                </View>
               
            </KeyboardAwareScrollView>
        );
    }

    _enviaPedido(){
        let nomeRestaurante = this.props.restaurante.titulo;
        this.props.gerenciaComanda({...this.props});
        Actions.comanda({title:nomeRestaurante});
    }
}



const styles = StyleSheet.create({
    tela: {
        flex: 1,
        padding:5
    },
    cabecalho:{
        flexDirection: 'row',
        padding: 10,
        flex: 1
    },
    tituloView:{
        flex: 1,
        alignItems: 'center'
    },
    titulo:{
        fontSize: 25,
        fontWeight: 'bold',
        color: 'black',
        textAlign: 'center'
    },
    imagemCatalogo: {
        height: 100,
        width: 100,
        borderRadius:  30
    },
    texto: {
        fontSize: 18,
        color: 'black'
    }, 
    descricaoView: {
        flex:4,
        alignItems: 'stretch',
    
    },
    quantidadeView:{
        paddingTop:5,
        paddingBottom:5,
        flex: 1, 
        flexDirection:'row'
    },
    quantidadeText:{ 
        flex: 3, 
        textAlign:'right', 
        fontSize: 18,
        color: 'black', 
        fontWeight: 'bold'
        
    },
    comentarioView:{
        flex: 4,
    },
    textInputComentarios: {
        borderWidth: 1,
        borderRadius:  4,
        textAlignVertical : 'top'
    },
    buttonView:{
        flex: 1, 
        justifyContent: 'flex-end'
    },
    button:{
        borderRadius:  8
    }
})

mapStateToProps = state => {
    return({
        comentarioPedido : state.AppReducer.comentarioPedido,
        quantidade: state.AppReducer.quantidade,
        valorTotal: state.AppReducer.valorTotal,
        pedido: state.ListaMenuReducer.ultimoItemMenuEscolhido,
        restaurante : state.ListaMenuReducer.restauranteEscolhido,
        idUsuario : state.AutenticacaoReducer.idUsuario,
        valorAtualComanda : state.AppReducer.valorAtualComanda,
        idComanda: state.AppReducer.idComanda
    })
}

export default connect(mapStateToProps, { modificaComentarioPedido, modificaQuantidadePedido, gerenciaComanda })(DescricaoPedido);