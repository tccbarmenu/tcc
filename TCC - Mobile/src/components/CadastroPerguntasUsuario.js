import React , { Component } from 'react';
import {View, TextInput, Button, StyleSheet, Image, Text, ActivityIndicator, Picker} from 'react-native';
import { connect } from 'react-redux';
import {modificaPerguntaUm,modificaPerguntaDois,modificaPerguntaTres,finalizaCadastroUsuario} from '../actions/AutenticacaoActions';

class CadastroPerguntasUsuario extends Component {

    _finalizaCadastroUsuario(){
        this.props.finalizaCadastroUsuario(this.props);
    }

    renderBtnCadastrar(){
        if(this.props.loadingCadastro){
            return (
                <ActivityIndicator size='large' color='white'/>
            );
        }
        return(
            <Button title='Finalizar Cadastro' color='#e20d0d' onPress={() => this._finalizaCadastroUsuario()} />
        );
    }

    render(){
        return(
            <Image style={{ flex:1, width: null}} source={ require('../imgs/Fundo2.png') } >
                <View style={styles.telaPrincipal} >
                    <View style={styles.telaFormCadastro} >
                        <Text style={styles.title}>Deixe-nos conhecer um pouco sobre você</Text>
                        <Text style={styles.textInputCadastro}>1 - Você prefere comidas doces ou salgadas?</Text>
                        <View style={{ borderColor:'#fff' , borderWidth:1, margin: 5 , height: 40}}>
                            <Picker
                                style={{color:'#fff'}}
                                selectedValue={this.props.perguntaDoceSalgado}
                                onValueChange={(itemValue, itemIndex) =>  {this.props.modificaPerguntaUm(itemValue)}}>
                                <Picker.Item label="Escolha" value="E" />
                                <Picker.Item label="Doce" value="Doce" />
                                <Picker.Item label="Salgado" value="Salgado" />
                            </Picker>
                        </View>
                        <Text style={styles.textInputCadastro}>2 - Possui algum tipo de restrição alimentar?</Text>
                        <View style={{ borderColor:'#fff',borderWidth:1, margin: 5 , height: 40}}>
                            <Picker
                                style={{color:'#fff'}}
                                selectedValue={this.props.perguntaRestricao}
                                onValueChange={(itemValue, itemIndex) =>  {this.props.modificaPerguntaDois(itemValue)}}>
                                <Picker.Item label="Escolha" value="E" />
                                <Picker.Item label="Nenhuma" value="Nenhuma" />
                                <Picker.Item label="Diabetes" value="Diabetes" />
                                <Picker.Item label="Hipertensão" value="Hipertensao" />
                                <Picker.Item label="Lactose" value="Lactose" />
                                <Picker.Item label="Glúten" value="Gluten" />
                                <Picker.Item label="Vegetariano" value="Vegetariano" />
                                <Picker.Item label="Vegano" value="Vegano" />
                            </Picker>
                        </View>
                        <Text style={styles.textInputCadastro}>3 - Qual seu tipo de restaurante favorito?</Text>
                        <View style={{ borderColor:'#fff',borderWidth:1, margin: 5 , height: 40}}>
                            <Picker
                                style={{color:'#fff'}}
                                selectedValue={this.props.perguntaTipoRestaurante}
                                onValueChange={(itemValue, itemIndex) =>  {this.props.modificaPerguntaTres(itemValue)}}>
                                <Picker.Item label="Escolha" value="E" />
                                <Picker.Item label="Churrascaria" value="Churrascaria" />
                                <Picker.Item label="Fast Food" value="Fast_Food" />
                                <Picker.Item label="Italiana" value="Italiana" />
                                <Picker.Item label="Alemã" value="Alema" />
                                <Picker.Item label="Mexicana" value="Mexicana" />
                                <Picker.Item label="Japonesa" value="Japonesa" />
                                <Picker.Item label="Chinesa" value="Chinesa" />
                            </Picker>
                        </View>
                        <Text style={styles.txtErro} > {this.props.erroCadastro}</Text>
                    </View>
                    <View style={styles.telaFormButton} >
                        { this.renderBtnCadastrar() }
                    </View>
                </View>
            </Image>
        );
    }
}

const mapStateToProps = state => (
    {
        idUsuario :state.AutenticacaoReducer.idUsuario,
        perguntaTipoRestaurante:state.AutenticacaoReducer.perguntaTipoRestaurante,
        perguntaDoceSalgado: state.AutenticacaoReducer.perguntaDoceSalgado,
        perguntaRestricao: state.AutenticacaoReducer.perguntaRestricao,
        erroCadastro: state.AutenticacaoReducer.erroCadastro,
        loadingCadastro: state.AutenticacaoReducer.loadingCadastro
    }
);
export default connect(mapStateToProps, {  modificaPerguntaUm,modificaPerguntaDois,modificaPerguntaTres,finalizaCadastroUsuario })(CadastroPerguntasUsuario);

const styles = StyleSheet.create({
    telaPrincipal: {
        flex: 1,
    },
    telaFormCadastro: {
        flex:6,
        justifyContent: 'center',
        padding: 10
    },
    title:{
         fontSize: 30,
         fontWeight:'bold',
         textAlign:'center',
        paddingHorizontal:5,
        paddingBottom: 15,
        color: '#fff'
    },
    textInputCadastro: {
        fontSize: 20,
        padding: 5,
        color: '#fff'
    },
    telaFormButton: {
        flex: 1,
        justifyContent:'flex-end'
    },
    txtErro: {
        color: '#ff0000',
        fontSize: 18
    }

});


