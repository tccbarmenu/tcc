import React, { PropTypes } from 'react';
import {Router, Scene } from 'react-native-router-flux';

import FormLogin from './components/FormLogin.js';
import FormCadastro from './components/FormCadastro.js';
import Restaurantes from './components/Restaurantes.js';
import Comanda from './components/Comanda.js';
import DescricaoPedido from './components/DescricaoPedido.js';
import Avaliacao from './components/Avaliacao.js';
import Pedido from './components/Pedido.js';
import CadastroPerguntasUsuario from './components/CadastroPerguntasUsuario.js';


export default props => (
    <Router navigationBarStyle={{ backgroundColor: '#e20d0d'}} titleStyle={{ color:'white', fontWeight: 'bold'}}> 
            <Scene key='formLogin'component={FormLogin} initial={true}   title="Login" hideNavBar={ true}/>
            <Scene key='formCadastro' component={FormCadastro} title="Cadastro"  hideNavBar={ false}/>
            <Scene key='restaurantes' component={Restaurantes}  title="Restaurantes"  hideNavBar={ true}  />
            <Scene key='comanda' component={Comanda} title="Comanda"  hideNavBar={ false}  />
            <Scene key='avaliacao' component={Avaliacao} title="Avaliação"  hideNavBar={ false}  />
            <Scene key='pedido' component={Pedido} title="Pedido"  hideNavBar={ false}  />
            <Scene key='CadastroPerguntasUsuario' component={CadastroPerguntasUsuario}  title="CadastroPerguntasUsuario"  hideNavBar={true}  />   
    </Router>
);

