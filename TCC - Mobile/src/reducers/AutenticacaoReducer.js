import {
    MODIFICA_EMAIL, 
    MODIFICA_SENHA, 
    MODIFICA_NOME, 
    CADASTRA_USUARIO_SUCESSO,
    CADASTRA_USUARIO_ERRO,
    LOGIN_USUARIO_SUCESSO,
    LOGIN_USUARIO_ERRO,
    LOGIN_EM_ANDAMENTO,
    CADASTRO_EM_ANDAMENTO,
    MODIFICA_DATA_NASCIMENTO,
    MODIFICA_SEXO,
    MODIFICA_CIDADE,
    CADASTRAO_SUCESSO_PRIMEIRA_ETAPA,
    MODIFICA_PERGUNTA_UM,
    MODIFICA_PERGUNTA_DOIS,
    MODIFICA_PERGUNTA_TRES,
    LOGOUT_SISTEMA
} from '../actions/types.js';

const INITIAL_STATE = {
    nome: '',
    email: 'marcelo@teste.com.br',
    senha: '123456',
    dataNascimento: '',
    cidade:'',
    sexo:'M',
    erroCadastro: '',
    perguntaDoceSalgado:'',
    perguntaRestricao: '',
    perguntaTipoRestaurante: '',
    idUsuario:'',
    erroLogin: '',
    loadingLogin: false,
    isTabViewAnimated: false,
    loadingCadastro: false
}

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case MODIFICA_PERGUNTA_UM:
            return { ...state, perguntaDoceSalgado: action.payload };
        case MODIFICA_PERGUNTA_DOIS:
            return { ...state, perguntaRestricao: action.payload };
        case MODIFICA_PERGUNTA_TRES:
            return { ...state, perguntaTipoRestaurante: action.payload };
        case MODIFICA_EMAIL:
            return { ...state, email: action.payload };
        case MODIFICA_SENHA:
            return { ...state, senha: action.payload };
        case MODIFICA_NOME:
            return { ...state, nome: action.payload };
        case MODIFICA_DATA_NASCIMENTO:
            return { ...state, dataNascimento: action.payload };
        case MODIFICA_SEXO:
            return { ...state, sexo: action.payload };
        case MODIFICA_CIDADE:
            return { ...state, cidade: action.payload };
        case CADASTRA_USUARIO_ERRO:
            return {...state, erroCadastro: action.payload, loadingCadastro: false};
        case CADASTRAO_SUCESSO_PRIMEIRA_ETAPA:
            return {...state, idUsuario:action.payload , erroCadastro:'', loadingCadastro: false};
        case CADASTRA_USUARIO_SUCESSO:
            return {...state, nome: '', senha: '',dataNascimento: '', cidade:'', sexo:'M',idUsuario:'', loadingCadastro: false};
        case LOGIN_USUARIO_ERRO:
            return {...state, erroLogin: action.payload, loadingLogin: false};
        case LOGIN_USUARIO_SUCESSO:
            return {...state, loadingLogin: false, email: '', senha: '',idUsuario:action.payload, isTabViewAnimated: false}
        case LOGIN_EM_ANDAMENTO:
            return {...state, loadingLogin: true};
        case CADASTRO_EM_ANDAMENTO:
            return {...state, loadingCadastro: true};
        case LOGOUT_SISTEMA:
            return {...state, idUsuario: ''};
        default:
            return state;

    }
    return state;
}