import {MODIFICA_COMENTARIO_PEDIDO,
    MODIFICA_QUANTIDADE,
    MODIFICA_VALOR,
    LIMPA_VARIAVEIS_PEDIDO,
    ENVIA_PEDIDO
} from '../actions/types';

const INITIAL_STATE = {
    comentarioPedido:'',
    quantidade: 1,
    valorTotal: 0.00,
    itemPedido: {},
    valorAtualComanda: 0.00,
    idComanda: ''
}

export default (state = INITIAL_STATE , action) => {
    switch(action.type){
        
        case MODIFICA_QUANTIDADE:
            return {...state,quantidade: action.payload.qtd, valorTotal: action.payload.valorTotal};
        case MODIFICA_COMENTARIO_PEDIDO:
            return {...state, comentarioPedido: action.payload}
        case LIMPA_VARIAVEIS_PEDIDO:
            return {...state, comentarioPedido: '',quantidade: 1}
        case ENVIA_PEDIDO: 
            return {...state,valorAtualComanda:action.payload.valorAtualComanda, idComanda:action.payload.idComanda }
        default:
            return state;
    }
}