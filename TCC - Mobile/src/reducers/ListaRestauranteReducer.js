import {LISTA_RESTAURANTES} from '../actions/types';

const INITIAL_STATE = {
    filtro_restaurante:'',
    page_restaurante: 0,
    info_restaurante: {}
}

export default (state = INITIAL_STATE , action) => {
    switch(action.type){
        case LISTA_RESTAURANTES:
            return {...state,info_restaurante: action.payload};
        default:
            return state;
    }
}