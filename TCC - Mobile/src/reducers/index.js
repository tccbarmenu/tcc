import { combineReducers } from 'redux';
import AutenticacaoReducer from './AutenticacaoReducer';
import ListaRestauranteReducer from './ListaRestauranteReducer';
import ListaMenuReducer from './ListaMenuReducer';
import ListaComandaReducer from './ListaComandaReducer';
import AppReducer from './AppReducer';

export default combineReducers({
    AutenticacaoReducer: AutenticacaoReducer,
    ListaRestauranteReducer:ListaRestauranteReducer,
    ListaMenuReducer: ListaMenuReducer,
    ListaComandaReducer: ListaComandaReducer,
    AppReducer:AppReducer
});