import {LISTA_MENU,
    SET_RESTAURANTE,
    ABRE_TELA_PEDIDO
} from '../actions/types';

const INITIAL_STATE = {
    page_menu: 0,
    info_menu: {},
    restauranteEscolhido: {},
    ultimoItemMenuEscolhido: {}
}

export default (state = INITIAL_STATE , action) => {
    switch(action.type){
        
        case LISTA_MENU:
            return {...state,info_menu: action.payload};
        case SET_RESTAURANTE:
            return {...state, restauranteEscolhido: action.payload}
        case ABRE_TELA_PEDIDO:
        return {...state, ultimoItemMenuEscolhido: action.payload}
        default:
            return state;
    }
}