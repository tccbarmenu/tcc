import {LISTA_COMANDA} from '../actions/types';

const INITIAL_STATE = {
    itens_comanda: {},
}

export default (state = INITIAL_STATE , action) => {
    switch(action.type){
        
        case LISTA_COMANDA:
            return {...state,itens_comanda: action.payload};
        default:
            return state;
    }
}