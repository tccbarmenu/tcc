# Sabino -| Spring Boot Restful :leaves:

## Quickstart

:octocat: From the command line do:
```javascript
$ git clone https://github.com/RicardoSabinolrs/spring-boot-restful.git
$ cd spring-boot-restful
$ ./gradlew build
$ java -jar /build/libs/spring-boot-restful-app-1.0.jar
```
> You can access the API documentation via browser: http://localhost:8080/swagger-ui.html

## Project description

The project uses:

- [Java 8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jre8-downloads-2133155.html)
- [Gradle](https://github.com/gradle/gradle)
- [Spring Boot](http://github.com/spring-projects/spring-boot)
- [Lambok](https://github.com/rzwitserloot/lombok)
- [Swagger](https://github.com/swagger-api)
- [jUnit](https://github.com/junit-team/junit4)
- [Mockito](https://github.com/mockito/mockito)

Some techniques:
- [Domain Driven Design](https://en.wikipedia.org/wiki/Domain-driven_design)
- [Dependency Injection](https://en.wikipedia.org/wiki/Dependency_injection)
- [TDD with Mocks and Stubs](https://en.wikipedia.org/wiki/Test-driven_development)

