package br.com.tcc.spring.boot.restful.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.tcc.spring.boot.restful.domain.entity.ItemMenu;

public interface ItemMenuRepository extends JpaRepository<ItemMenu, Long> {
	 @Query("SELECT item FROM ItemMenu item where idEstabelecimento = :idEstabelecimento")
	    public List<ItemMenu> getAllItemMenuByRestaurante(@Param("idEstabelecimento") Long idEstabelecimento);
}
