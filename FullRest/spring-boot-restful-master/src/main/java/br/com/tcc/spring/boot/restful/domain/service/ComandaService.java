package br.com.tcc.spring.boot.restful.domain.service;

import org.springframework.stereotype.Service;

import br.com.tcc.spring.boot.restful.domain.entity.Comanda;
import br.com.tcc.spring.boot.restful.repository.ComandaRepository;

import java.util.List;

@Service
public class ComandaService {

    private final ComandaRepository repository;

    public ComandaService(ComandaRepository repository) {
        this.repository = repository;
    }

    public Comanda getComandaById(Long id) {
        return repository.findOne(id);
    }

    public List<Comanda> getAllComandas() {
        return repository.findAll();
    }

    public Long addComanda(Comanda comanda) {
        return repository.save(comanda).getIdComanda();
    }

    public Long updateComanda(Long id, Comanda comanda) {
    	Comanda comandaAtualizar = repository.findOne(id);
        comandaAtualizar.setIdUsuario(comanda.getIdUsuario());
        comandaAtualizar.setMesa(comanda.getMesa());
        comandaAtualizar.setValorTotal(comanda.getValorTotal());
        return repository.save(comandaAtualizar).getIdComanda();
    }
    
    public Long updateValorTotalComanda(Long id, Comanda comanda) {
    	Comanda comandaAtualizar = repository.findOne(id);
        comandaAtualizar.setValorTotal(comanda.getValorTotal());
        return repository.save(comandaAtualizar).getIdComanda();
    }

    public void removeAllComanda() {
        repository.deleteAll();
    }

    public void removeComandaById(Long id) {
        repository.delete(id);
    }
}
