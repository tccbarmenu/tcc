package br.com.tcc.spring.boot.restful.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
@Data
@Entity
@Table
public class Avaliacao {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID_AVALIACAO")
    private Long idAvaliacao;
	
	@Column(name="ID_USUARIO")
    private Long idUsuario;
	
	@Valid
    @Column(name="ID_ITEM")
    private Long idItem;
    
    @Column(name="NOTA")
    private Float nota;
   
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name="data_registro")
    private Date dataCadastro;
}
