package br.com.tcc.spring.boot.restful.domain.service;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.jdbc.MySQLJDBCDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

import br.com.tcc.spring.boot.restful.domain.entity.Recomendacao;
import br.com.tcc.spring.boot.restful.repository.RecomendacaoRepository;

@Service
public class RecomendacaoService implements ApplicationListener<ApplicationReadyEvent>{

    private final RecomendacaoRepository repository;
    
    @Autowired
    private DataSource dataSource;
    
    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event)  {
    	criaTabelaRecomendacao();
    	return;
    }

    public RecomendacaoService(RecomendacaoRepository repository) {
        this.repository = repository;
    }

    public Recomendacao getRecomendacaoById(Long id) {
        return repository.findOne(id);
    }
    
    public Recomendacao findRecomendacaoToUserByIdEstabelecimento(Long idUsuario, Long idEstabelecimento) {
		return repository.findRecomendacaoToUserByIdEstabelecimento(idUsuario, idEstabelecimento);
	}
    
    public Long updateRecomendacao(Long id, Recomendacao recomendacao) {
    	Recomendacao recomendacaoAtualizar = repository.findOne(id);
        recomendacaoAtualizar.setIdUsuario(recomendacao.getIdUsuario());
        recomendacaoAtualizar.setIdEstabelecimento(recomendacao.getIdEstabelecimento());
        recomendacaoAtualizar.setIdItem(recomendacao.getIdItem());
        recomendacaoAtualizar.setNota(recomendacao.getNota());
        return repository.save(recomendacaoAtualizar).getIdRecomendacao();
    }

    public List<Recomendacao> getAllRecomendacoes() {
    	criaTabelaRecomendacao() ;
        return repository.findAll();
    }

    public Long addRecomendacao(Recomendacao recomendacao) {
        return repository.save(recomendacao).getIdRecomendacao();
    }

    public void removeAllRecomendacao() {
        repository.deleteAll();
    }

    public void removeRecomendacaooById(Long id) {
        repository.delete(id);
    }
    
    
    private void criaTabelaRecomendacao() {
    	repository.deleteAll();
    	List<Long> idUsuarioList = repository.getAllIdUsuario();
    	for(Long idUsuario : idUsuarioList) {
    		List<RecommendedItem> itensRecomendados = geraRecomendacoes(idUsuario);
    		for(RecommendedItem recomendation : itensRecomendados) {
    			Recomendacao recomendacaoAtualizar = new Recomendacao();
    			recomendacaoAtualizar.setIdUsuario(idUsuario);
    			recomendacaoAtualizar.setIdEstabelecimento(repository.getIdEstabelecimento(recomendation.getItemID()));
    			recomendacaoAtualizar.setIdItem(recomendation.getItemID());
    			recomendacaoAtualizar.setNota(recomendation.getValue());
    			repository.save(recomendacaoAtualizar).getIdRecomendacao();
    		}
    	}
	}
    
    public List<RecommendedItem>  geraRecomendacoes(Long idUsuario)  {
    	DataModel avaliacaoModel = new MySQLJDBCDataModel(dataSource, "Avaliacao", "ID_USUARIO", "ID_ITEM","NOTA","data_registro");
		List<RecommendedItem> itensRecomendados = new ArrayList<>();
		Recommender recommender;
		try {
			recommender = buildRecommender(avaliacaoModel);
		
			List<RecommendedItem> recommendations =recommender.recommend(idUsuario, 100);
			for(RecommendedItem recomendation : recommendations) {
				itensRecomendados.add(recomendation);
			}
		} catch (TasteException e) {
			e.printStackTrace();
		}
		return itensRecomendados;
	}

    
    public Recommender buildRecommender(DataModel model) throws TasteException {
		UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
		
		UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1, similarity, model);
		UserBasedRecommender recommender =new GenericUserBasedRecommender(model, neighborhood, similarity);
		
		return recommender;
	}
    
    
}
