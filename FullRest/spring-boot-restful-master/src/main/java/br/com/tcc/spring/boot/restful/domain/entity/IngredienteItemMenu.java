//package br.com.tcc.spring.boot.restful.domain.entity;
//
//import java.io.Serializable;
//import java.util.Date;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//import javax.validation.Valid;
//
//import org.hibernate.annotations.CreationTimestamp;
//import org.hibernate.validator.constraints.NotBlank;
//
//import com.fasterxml.jackson.annotation.JsonFormat;
//
//import lombok.Data;
//@Data
//@Entity
//@Table(name="Item_menuXIngrediente")
//public class IngredienteItemMenu implements Serializable{
//	
//	@Id
//	@Column(name="ID_INGREDIENTE")
//    private Long idIngrediente;
//	
//	@Id
//    @Column(name="ID_ITEM_MENU")
//    private Long idItemMenu;
//	
//    @CreationTimestamp
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
//    @Column(name="data_registro")
//    private Date dataCadastro;
//  
//}
