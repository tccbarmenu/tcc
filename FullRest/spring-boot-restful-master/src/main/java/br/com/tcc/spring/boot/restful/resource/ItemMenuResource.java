package br.com.tcc.spring.boot.restful.resource;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tcc.spring.boot.restful.domain.entity.ItemMenu;
import br.com.tcc.spring.boot.restful.domain.service.ItemMenuService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/itemMenu")
@Api(value = "Bar Menu Restful", description = "Itens do Menu")
public class ItemMenuResource {

    private final ItemMenuService service;

    public ItemMenuResource(ItemMenuService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todos os itens Menu")
    @ApiResponse(code = 200, message = "Ok", response = ItemMenu.class)
    //@Cacheable(value = "itemMenus")
    @GetMapping
    public ResponseEntity<List<ItemMenu>> getAll() {
        return new ResponseEntity<>(service.getAllItensMenu(), HttpStatus.OK);
    }

    @ApiOperation(value = "Obtem todos os itens Menu por estabelecimento")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "itemMenu", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<List<ItemMenu>> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getAllItemMenuByRestaurante(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra item menu")
    @ApiResponse(code = 201, message = "Created", response = ItemMenu.class)
    //@Cacheable(value = "itemMenus")
   // @CacheEvict(cacheNames = "itemMenus", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody ItemMenu itemMenu) {
        return new ResponseEntity<>(service.addItemMenu(itemMenu), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Atualiza/Cria um item menu")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "itemMenus")
   // @CacheEvict(cacheNames = "itemMenus", allEntries = true)
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> put(@PathVariable("id") Long id, @RequestBody ItemMenu itemMenu) {
        return new ResponseEntity<>(service.updateItemMenu(id, itemMenu), HttpStatus.CREATED);
    }

    @ApiOperation(value =  "Remove um item menu")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"itemMenus", "itemMenu"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeById(@PathVariable("id") Long id) {
        service.removeItemMenuById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todos os itens menus")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"itemMenus", "itemMenu"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAll() {
        service.removeAllItensMenu();
        return ResponseEntity.noContent().build();
    }
}
