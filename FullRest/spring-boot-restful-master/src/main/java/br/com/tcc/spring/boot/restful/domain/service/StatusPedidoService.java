package br.com.tcc.spring.boot.restful.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.tcc.spring.boot.restful.domain.entity.StatusPedido;
import br.com.tcc.spring.boot.restful.repository.StatusPedidoRepository;

@Service
public class StatusPedidoService {

    private final StatusPedidoRepository repository;

    public StatusPedidoService(StatusPedidoRepository repository) {
        this.repository = repository;
    }

    public StatusPedido getCategoriaById(Long id) {
        return repository.findOne(id);
    }

    public List<StatusPedido> getAllCategorias() {
        return repository.findAll();
    }

    public Long addCategoria(StatusPedido status) {
        return repository.save(status).getIdStatusPedido();
    }
    
    public Long getStatusByEstabelecimento(String nome, Long idEstabelecimento) {
        return repository.getStatusPedidoByEstabelecimento(nome, idEstabelecimento);
    }

    public Long updateCategoria(Long id, StatusPedido status) {
    	StatusPedido statusAtualizar = repository.findOne(id);
        statusAtualizar.setNome(status.getNome());
        statusAtualizar.setDescricao(status.getDescricao());
        statusAtualizar.setIdEstabelecimento(status.getIdEstabelecimento());
        return repository.save(statusAtualizar).getIdStatusPedido();
    }

    public void removeAllCategoria() {
        repository.deleteAll();
    }

    public void removeCategoriaById(Long id) {
        repository.delete(id);
    }
}
