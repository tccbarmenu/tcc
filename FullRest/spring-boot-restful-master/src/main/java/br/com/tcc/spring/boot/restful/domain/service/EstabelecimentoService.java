package br.com.tcc.spring.boot.restful.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.tcc.spring.boot.restful.domain.entity.Estabelecimento;
import br.com.tcc.spring.boot.restful.repository.EstabelecimentoRepository;

@Service
public class EstabelecimentoService {

    private final EstabelecimentoRepository repository;

    public EstabelecimentoService(EstabelecimentoRepository repository) {
        this.repository = repository;
    }

    public Estabelecimento getEstabelecimentoById(Long id) {
        return repository.findOne(id);
    }

    public List<Estabelecimento> getAllEstabelecimentos() {
        return repository.findAll();
    }

    public Long addEstabelecimento(Estabelecimento estabelecimento) {
        return repository.save(estabelecimento).getIdEstabelecimento();
    }

    public Long updateEstabelecimento(Long id, Estabelecimento newLocal) {
    	Estabelecimento estabelecimentoAtualizar = repository.findOne(id);
        estabelecimentoAtualizar.setNomeEstabelecimento(newLocal.getNomeEstabelecimento());
        estabelecimentoAtualizar.setDescricaoEstabelecimento(newLocal.getDescricaoEstabelecimento());
        estabelecimentoAtualizar.setEndereco(newLocal.getEndereco());
        estabelecimentoAtualizar.setCidade(newLocal.getCidade());
        return repository.save(estabelecimentoAtualizar).getIdEstabelecimento();
    }

    public void removeAllEstabelecimentos() {
        repository.deleteAll();
    }

    public void removeEstabelecimentoById(Long id) {
        repository.delete(id);
    }
}
