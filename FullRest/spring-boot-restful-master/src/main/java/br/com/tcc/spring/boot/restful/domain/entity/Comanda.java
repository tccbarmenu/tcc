package br.com.tcc.spring.boot.restful.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
public class Comanda {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_comanda")
    private Long idComanda;
    
    @Valid
    @Column(name="id_status_comanda")
    private Long statusComanda;
    
    @Column(name="valor_total")
    private String valorTotal;
    
    @Valid
    @Column(name="id_usuario")
    private Long idUsuario;
    
    @Valid
    @Column(name="id_estabelecimento")
    private Long idEstabelecimento;
    
    
    @Column(name="mesa")
    private String mesa;
   
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name="data_registro")
    private Date dataCadastro;
    
}
