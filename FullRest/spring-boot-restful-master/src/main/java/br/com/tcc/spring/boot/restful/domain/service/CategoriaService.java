package br.com.tcc.spring.boot.restful.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.tcc.spring.boot.restful.domain.entity.Categoria;
import br.com.tcc.spring.boot.restful.repository.CategoriaRepository;

@Service
public class CategoriaService {

    private final CategoriaRepository repository;

    public CategoriaService(CategoriaRepository repository) {
        this.repository = repository;
    }

    public Categoria getCategoriaById(Long id) {
        return repository.findOne(id);
    }

    public List<Categoria> getAllCategorias() {
        return repository.findAll();
    }

    public Long addCategoria(Categoria categoria) {
        return repository.save(categoria).getIdCategoria();
    }

    public Long updateCategoria(Long id, Categoria categoria) {
    	Categoria categoriaAtualizar = repository.findOne(id);
        categoriaAtualizar.setNome(categoria.getNome());
        categoriaAtualizar.setDescricao(categoria.getDescricao());
        categoriaAtualizar.setIdEstabelecimento(categoria.getIdEstabelecimento());
        return repository.save(categoriaAtualizar).getIdCategoria();
    }

    public void removeAllCategoria() {
        repository.deleteAll();
    }

    public void removeCategoriaById(Long id) {
        repository.delete(id);
    }
}
