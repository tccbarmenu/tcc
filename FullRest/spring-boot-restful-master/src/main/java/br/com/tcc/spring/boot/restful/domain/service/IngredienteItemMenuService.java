//package br.com.tcc.spring.boot.restful.domain.service;
//
//import java.util.List;
//
//import org.springframework.stereotype.Service;
//
//import br.com.tcc.spring.boot.restful.domain.entity.IngredienteItemMenu;
//import br.com.tcc.spring.boot.restful.repository.IngredienteItemMenuRepository;
//
//
//@Service
//public class IngredienteItemMenuService {
//
//    private final IngredienteItemMenuRepository repository;
//
//    public IngredienteItemMenuService(IngredienteItemMenuRepository repository) {
//        this.repository = repository;
//    }
//
//    public IngredienteItemMenu getRelacaoPorIdIngrediente(Long id) {
//        return repository.findOne(id);
//    }
//
//    public List<IngredienteItemMenu> getTodasRelacoes() {
//        return repository.findAll();
//    }
//
//    public Long addRelacao(IngredienteItemMenu ingredienteItemMenu) {
//        return repository.save(ingredienteItemMenu).getIdIngrediente();
//    }
//
//    public Long alteraRelacaoPorIdIngrediente(Long idIngrediente, IngredienteItemMenu relacao) {
//    	IngredienteItemMenu itemAtualizar = repository.findOne(idIngrediente);
//        itemAtualizar.setIdItemMenu(relacao.getIdItemMenu());
//        return repository.save(itemAtualizar).getIdIngrediente();
//    }
//
//    public void removeRelacao() {
//        repository.deleteAll();
//    }
//
//    public void removeRelacaoPorIdIngrediente(Long idIngrediente) {
//        repository.delete(idIngrediente);
//    }
//}
