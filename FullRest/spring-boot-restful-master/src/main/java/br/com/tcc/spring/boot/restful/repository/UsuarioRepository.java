package br.com.tcc.spring.boot.restful.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.tcc.spring.boot.restful.domain.entity.Usuario;


public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	 @Query("SELECT idUsuario FROM Usuario user where user.idUsuarioFirebase = :idUsuarioFirebase")
	    public Long getUsuarioByIdUsuarioFirebase(@Param("idUsuarioFirebase") String idUsuarioFirebase);
}
