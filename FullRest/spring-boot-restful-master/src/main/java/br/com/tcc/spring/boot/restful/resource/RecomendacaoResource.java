package br.com.tcc.spring.boot.restful.resource;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.tcc.spring.boot.restful.domain.entity.Recomendacao;
import br.com.tcc.spring.boot.restful.domain.service.RecomendacaoService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/recomendacao")
@Api(value = "Bar Menu Restful", description = "Categoria API")
public class RecomendacaoResource {

    private final RecomendacaoService service;
    

    public RecomendacaoResource(RecomendacaoService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todas as recomenda��es")
    @ApiResponse(code = 200, message = "Ok", response = Recomendacao.class)
    //@Cacheable(value = "recomendacoes")
    @GetMapping
    public ResponseEntity<List<Recomendacao>> getAll() {
        return new ResponseEntity<>(service.getAllRecomendacoes(), HttpStatus.OK);
    }

    @ApiOperation(value = "Obtem recomenda��o por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "recomendacao", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Recomendacao> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getRecomendacaoById(id), HttpStatus.OK);
    }
    
    @ApiOperation(value = "Obtem recomenda��o para usu�rio por Estabelecimento")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "recomendacao", key = "#id")
    @GetMapping(value = "byEstabelecimento")
    public ResponseEntity<Recomendacao> getToUsuarioByEstabelecimento(@RequestParam ("idUsuario") Long idUsuario, @RequestParam ("idEstabelecimento") Long idEstabelecimento) {
        return new ResponseEntity<>(service.findRecomendacaoToUserByIdEstabelecimento(idUsuario, idEstabelecimento), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra recomenda��o")
    @ApiResponse(code = 201, message = "Created", response = Recomendacao.class)
    //@Cacheable(value = "recomendacoes")
   // @CacheEvict(cacheNames = "recomendacoes", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody Recomendacao recomendacao) {
        return new ResponseEntity<>(service.addRecomendacao(recomendacao), HttpStatus.CREATED);
    }
    
    @ApiOperation(value = "Atualiza/Cria uma recomenda��o")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "recomendacoes")
   // @CacheEvict(cacheNames = "recomendacoes", allEntries = true)
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> put(@PathVariable("id") Long id, @RequestBody Recomendacao recomendacao) {
        return new ResponseEntity<>(service.updateRecomendacao(id, recomendacao), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Remove uma recomenda��o")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"recomendacoes", "recomendacao"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeAvaliacaoById(@PathVariable("id") Long id) {
        service.removeRecomendacaooById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todas as recomenda��es")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"recomendacoes", "recomendacao"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAllAvaliacoes() {
        service.removeAllRecomendacao();
        return ResponseEntity.noContent().build();
    }
}
