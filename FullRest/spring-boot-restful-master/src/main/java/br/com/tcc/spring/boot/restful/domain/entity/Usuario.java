package br.com.tcc.spring.boot.restful.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
@Data
@Entity
@Table(name="USUARIOS")
public class Usuario {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    @Column(name="ID_USUARIO")
    private Long idUsuario;
    
	@Column(name="ID_USUARIO_FIREBASE")
    private String idUsuarioFirebase;
	
    @Column(name="id_niveis_acesso")
    private Long niveAcesso;
	
	@Valid
	@Column(name="nome_usuario")
    private String nome;
	
	@Column(name="id_estabelecimento")
    private String idEstabelecimento;
	
	@Valid
    @Column(name="DATA_NASCIMENTO")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private String dataNascimento;
	
	@Valid
	@Column(name="login_usuario")
    private String login;
	
	@Valid
	@Column(name="senha_usuario")
    private String senha;
	
	@Valid
	@Column(name="email_usuario")
    private String email;
	
	@Valid
	@Column(name="cidade_usuario")
    private String cidade;
	
	@Valid
	@Column(name="sexo_usuario")
    private String sexo;
	
	@Column(name="pergunta_um")
    private String perguntaUm;
	
	@Column(name="pergunta_dois")
    private String perguntaDois;
	
	@Column(name="pergunta_tres")
    private String perguntaTres;
   
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name="data_registro")
    private Date dataCadastro;
}
