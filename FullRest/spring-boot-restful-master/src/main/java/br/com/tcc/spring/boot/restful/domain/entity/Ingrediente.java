package br.com.tcc.spring.boot.restful.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.Valid;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
@Data
@Entity
public class Ingrediente {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID_INGREDIENTE")
    private Long id;
	
	@Valid
	@Column(name="nome_ingrediente")
    private String nome;
    
    @Valid
    @Column(name="ID_ESTABELECIMENTO")
    private Long idEstabelecimento;

	@Column(name="descricao_ingrediente")
    private String Descricao;
   
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name="data_registro")
    private Date dataCadastro;
    
    
}
