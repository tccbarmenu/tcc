package br.com.tcc.spring.boot.restful.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.tcc.spring.boot.restful.domain.entity.Avaliacao;

public interface AvaliacaoRepository extends JpaRepository<Avaliacao, Long> {
}
