package br.com.tcc.spring.boot.restful.resource;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tcc.spring.boot.restful.domain.entity.Pedido;
import br.com.tcc.spring.boot.restful.domain.service.PedidoService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/pedido")
@Api(value = "Bar Menu Restful", description = "Pedido API")
public class PedidoResource {

    private final PedidoService service;

    public PedidoResource(PedidoService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todas os pedidos")
    @ApiResponse(code = 200, message = "Ok", response = Pedido.class)
    //@Cacheable(value = "categorias")
    @GetMapping
    public ResponseEntity<List<Pedido>> getAll() {
        return new ResponseEntity<>(service.getAllPedidos(), HttpStatus.OK);
    }
    
    @ApiOperation(value = "Obtem todos os pedidos de uma comanda")
    @ApiResponse(code = 200, message = "Ok", response = Pedido.class)
    //@Cacheable(value = "categorias")
    @GetMapping(value = "pedidoAgrupado")
    public ResponseEntity<List<Pedido>> findComandaCompleta(Long idComanda) {
        return new ResponseEntity<>(service.findPedidoAgrupado(idComanda), HttpStatus.OK);
    }
    
    
    

    @ApiOperation(value = "Obtem categoria por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "categoria", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Pedido> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getPedidoById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra categoria")
    @ApiResponse(code = 201, message = "Created", response = Pedido.class)
    //@Cacheable(value = "categorias")
   // @CacheEvict(cacheNames = "categorias", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody Pedido pedido) {
        return new ResponseEntity<>(service.addPedido(pedido), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Atualiza/Cria uma categoria")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "categorias") // @CacheEvict(cacheNames = "categorias", allEntries = true)
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> put(@PathVariable("id") Long id, @RequestBody Pedido pedido) {
        return new ResponseEntity<>(service.updatePedido(id, pedido), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Remove uma categoria")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"categorias", "categoria"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeById(@PathVariable("id") Long id) {
        service.removePedidoById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todas os pedidos")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"categorias", "categoria"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAll() {
        service.removeAllPedido();
        return ResponseEntity.noContent().build();
    }
}
