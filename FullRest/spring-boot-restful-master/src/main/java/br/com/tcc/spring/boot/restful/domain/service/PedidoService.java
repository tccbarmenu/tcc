package br.com.tcc.spring.boot.restful.domain.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.tcc.spring.boot.restful.domain.entity.ItemMenu;
import br.com.tcc.spring.boot.restful.domain.entity.Pedido;
import br.com.tcc.spring.boot.restful.repository.ItemMenuRepository;
import br.com.tcc.spring.boot.restful.repository.PedidoRepository;


@Service
public class PedidoService {

    private final PedidoRepository repository;
    
    @Autowired
    private final ItemMenuRepository repositoryItemMenu;
    @Autowired
    DataSource dataSource;

    public PedidoService(PedidoRepository repository, ItemMenuRepository repositoryItemMenu) {
        this.repository = repository;
        this.repositoryItemMenu = repositoryItemMenu;
    }

    public Pedido getPedidoById(Long id) {
        return repository.findOne(id);
    }

    public List<Pedido> getAllPedidos() {
        return repository.findAll();
    }
    
    public List<Pedido> findPedidoAgrupado(Long idComanda) {
    	List<Pedido> pedidosList = repository.findPedidoAgrupado(idComanda);
    	HashMap<Long, Long> quantidadeItensMenu = new HashMap<>();
    	for(Pedido pedido : pedidosList) {
    		Long quantidade = quantidadeItensMenu.get(pedido.getIdItemMenu()) != null ? quantidadeItensMenu.get(pedido.getIdItemMenu()) : 0;
    		quantidadeItensMenu.put(pedido.getIdItemMenu(), quantidade + pedido.getQuantidade());
    	}
    	List<Pedido> pedidoFinalList = new ArrayList<>();
    	for(Pedido pedido : pedidosList) {
    		Long idItemMenu = pedido.getIdItemMenu();
			if(quantidadeItensMenu.containsKey(idItemMenu)) {
    			ItemMenu itemMenu =  repositoryItemMenu.findOne(idItemMenu);
    			pedido.setQuantidade(quantidadeItensMenu.get(idItemMenu));
    			pedido.setNomePedido(itemMenu.getTitulo());
    			pedido.setValorTotalPedidoAgrupado(Float.valueOf(itemMenu.getPreco()) * quantidadeItensMenu.get(idItemMenu) );
    			pedidoFinalList.add(pedido);
    			quantidadeItensMenu.remove(idItemMenu);
    		}
    	}
    	
        return pedidoFinalList;
    }
    
    public Long updatePedido(Long id, Pedido pedido) {
    	Pedido pedidoAtualizar = repository.findOne(id);
        pedidoAtualizar.setIdComanda(pedido.getIdComanda());
        pedidoAtualizar.setIdEstabelecimento(pedido.getIdEstabelecimento());
        pedidoAtualizar.setIdItemMenu(pedido.getIdItemMenu());
        pedidoAtualizar.setIdStatusPedido(pedido.getIdStatusPedido());
        pedidoAtualizar.setObservacaoPedido(pedido.getObservacaoPedido());
        return repository.save(pedidoAtualizar).getIdPedido();
    }

    public Long addPedido(Pedido categoria) {
        return repository.save(categoria).getIdPedido();
    }

    public void removeAllPedido() {
        repository.deleteAll();
    }

    public void removePedidoById(Long id) {
        repository.delete(id);
    }
    
}
