package br.com.tcc.spring.boot.restful.resource;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.tcc.spring.boot.restful.domain.entity.Recomendacao;
import br.com.tcc.spring.boot.restful.domain.entity.Usuario;
import br.com.tcc.spring.boot.restful.domain.service.UsuarioService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/usuario")
@Api(value = "Bar Menu Restful", description = "Usuario API")
public class UsuarioResource {

    private final UsuarioService service;

    public UsuarioResource(UsuarioService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todas os usuarios")
    @ApiResponse(code = 200, message = "Ok", response = Usuario.class)
    //@Cacheable(value = "beers")
    @GetMapping
    public ResponseEntity<List<Usuario>> getAll() {
        return new ResponseEntity<>(service.getAllUsuarios(), HttpStatus.OK);
    }

    @ApiOperation(value = "Obtem usuario pelo id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "beer", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Usuario> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getUsuarioById(id), HttpStatus.OK);
    }
    
    @ApiOperation(value = "Obtem ID usuario pelo id Firebase")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "recomendacao", key = "#id")
    @GetMapping(value ="byIdFirebase")
    public ResponseEntity<Long> getToUsuarioByEstabelecimento(@RequestParam ("idUsuarioFirebase") String idUsuarioFirebase) {
        return new ResponseEntity<>(service.getUsuarioByIdUsuarioFirebase(idUsuarioFirebase), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra usuario")
    @ApiResponse(code = 201, message = "Created", response = Usuario.class)
    //@Cacheable(value = "beers")
   // @CacheEvict(cacheNames = "beers", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody Usuario beer) {
        return new ResponseEntity<>(service.addUsuarios(beer), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Atualiza/Cria um usuario")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "beers")
   // @CacheEvict(cacheNames = "beers", allEntries = true)
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> put(@PathVariable("id") Long id, @RequestBody Usuario beer) {
        return new ResponseEntity<>(service.updateUsuario(id, beer), HttpStatus.CREATED);
    }

    @ApiOperation(value =  "Remove u usuario")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"beers", "beer"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeById(@PathVariable("id") Long id) {
        service.removeUsuarioById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todas os usuarios")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"beers", "beer"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAll() {
        service.removeAllUsuarios();
        return ResponseEntity.noContent().build();
    }
}
