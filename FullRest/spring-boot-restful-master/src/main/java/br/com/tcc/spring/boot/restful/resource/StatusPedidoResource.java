package br.com.tcc.spring.boot.restful.resource;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.tcc.spring.boot.restful.domain.entity.StatusPedido;
import br.com.tcc.spring.boot.restful.domain.service.StatusPedidoService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/statusPedido")
@Api(value = "Bar Menu Restful", description = "Status Pedido API")
public class StatusPedidoResource {

    private final StatusPedidoService service;

    public StatusPedidoResource(StatusPedidoService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todos os status dos pedidos")
    @ApiResponse(code = 200, message = "Ok", response = StatusPedido.class)
    //@Cacheable(value = "statusPedidos")
    @GetMapping
    public ResponseEntity<List<StatusPedido>> getAll() {
        return new ResponseEntity<>(service.getAllCategorias(), HttpStatus.OK);
    }

    @ApiOperation(value = "Obtem status por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "statusPedido", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<StatusPedido> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getCategoriaById(id), HttpStatus.OK);
    }
    
    @ApiOperation(value = "Obtem status do pedido por Estabelecimento")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "recomendacao", key = "#id")
    @GetMapping(value = "byEstabelecimento")
    public ResponseEntity<Long> getToUsuarioByEstabelecimento(@RequestParam ("nome") String nome, @RequestParam ("idEstabelecimento") Long idEstabelecimento) {
        return new ResponseEntity<>(service.getStatusByEstabelecimento(nome, idEstabelecimento), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra status pedido")
    @ApiResponse(code = 201, message = "Created", response = StatusPedido.class)
    //@Cacheable(value = "statusPedidos")
   // @CacheEvict(cacheNames = "statusPedidos", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody StatusPedido statusPedido) {
        return new ResponseEntity<>(service.addCategoria(statusPedido), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Atualiza/Cria um status")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "statusPedidos")
   // @CacheEvict(cacheNames = "statusPedidos", allEntries = true)
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> put(@PathVariable("id") Long id, @RequestBody StatusPedido statusPedido) {
        return new ResponseEntity<>(service.updateCategoria(id, statusPedido), HttpStatus.CREATED);
    }

    @ApiOperation(value =  "Remove um status")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"statusPedidos", "statusPedido"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeById(@PathVariable("id") Long id) {
        service.removeCategoriaById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todos os status")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"statusPedidos", "statusPedido"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAll() {
        service.removeAllCategoria();
        return ResponseEntity.noContent().build();
    }
}
