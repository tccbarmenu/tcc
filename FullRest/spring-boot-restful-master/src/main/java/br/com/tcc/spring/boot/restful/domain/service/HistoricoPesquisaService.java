package br.com.tcc.spring.boot.restful.domain.service;

import org.springframework.stereotype.Service;

import br.com.tcc.spring.boot.restful.domain.entity.HistoricoPesquisa;
import br.com.tcc.spring.boot.restful.repository.HistoricoPesquisaRepository;

import java.util.List;

@Service
public class HistoricoPesquisaService {

    private final HistoricoPesquisaRepository repository;

    public HistoricoPesquisaService(HistoricoPesquisaRepository repository) {
        this.repository = repository;
    }

    public HistoricoPesquisa getHistoricoById(Long id) {
        return repository.findOne(id);
    }

    public List<HistoricoPesquisa> getAllHistoricos() {
        return repository.findAll();
    }

    public Long addHistorico(HistoricoPesquisa historico) {
        return repository.save(historico).getIdHistoricoPesquisa();
    }

    public Long updateHistorico(Long id, HistoricoPesquisa historico) {
    	HistoricoPesquisa historicoAtualizar = repository.findOne(id);
        historicoAtualizar.setIdUsuario(historico.getIdUsuario());
        historicoAtualizar.setCategoria(historico.getCategoria());
        historicoAtualizar.setFoiPedido(historico.getFoiPedido());
        historicoAtualizar.setIngredientesMantidos(historico.getIngredientesMantidos());
        historicoAtualizar.setIngredientesRemovidos(historico.getIngredientesRemovidos());
        historicoAtualizar.setNomePrato(historico.getNomePrato());
        historicoAtualizar.setValorPrato(historico.getValorPrato());
        return repository.save(historicoAtualizar).getIdHistoricoPesquisa();
    }

    public void removeAllHistorico() {
        repository.deleteAll();
    }

    public void removeHistoricoById(Long id) {
        repository.delete(id);
    }
}
