package br.com.tcc.spring.boot.restful.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.tcc.spring.boot.restful.domain.entity.HistoricoPesquisa;

public interface HistoricoPesquisaRepository extends JpaRepository<HistoricoPesquisa, Long> {
}
