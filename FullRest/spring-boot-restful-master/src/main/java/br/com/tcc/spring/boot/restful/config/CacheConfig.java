package br.com.tcc.spring.boot.restful.config;

import com.google.common.cache.CacheBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CacheConfig {
    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        GuavaCache beerCache = new GuavaCache("beer", CacheBuilder.newBuilder()
                .maximumSize(20).build());
        GuavaCache beersCache = new GuavaCache("beers", CacheBuilder.newBuilder()
                .maximumSize(20).expireAfterAccess(20, TimeUnit.MINUTES).build());
        cacheManager.setCaches(Arrays.asList(beerCache, beersCache));
        return cacheManager;
    }
}
