package br.com.tcc.spring.boot.restful.domain.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name="item_menu")
public class ItemMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID_ITEM_MENU" , updatable = false, nullable = false)
    private Long id;
    
    @Valid
    @Column(name="ID_ESTABELECIMENTO")
    private Long idEstabelecimento;
    
    @Column(name="SERVE_QTD_PESSOA")
    private Long serveQtdPessoas;
    
    @Column(name="id_categoria")
    private Long idCategoria;
    
    @Valid
    @Column(name="imagem_item_menu")
    private String imagem;
    
    @Column(name="descricao_item_menu")
    private String descricao;

    @Valid
    @Column(name="nome_item_menu")
    private String titulo;
    
    @Valid
    @Column(name="preco_item_menu")
    private String preco;
    
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name="data_registro")	
    private Date dataCadastro;
    
    @ManyToMany(cascade = CascadeType.DETACH ,fetch=FetchType.LAZY)
	@JoinTable(name = "item_menuxingrediente",
	joinColumns = { @JoinColumn(name = "ID_ITEM_MENU") },
	inverseJoinColumns = { @JoinColumn(name = "ID_INGREDIENTE") })
    private Set<Ingrediente> ingredientes ;
}
