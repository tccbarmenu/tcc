package br.com.tcc.spring.boot.restful.resource;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tcc.spring.boot.restful.domain.entity.Categoria;
import br.com.tcc.spring.boot.restful.domain.service.CategoriaService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/categoria")
@Api(value = "Bar Menu Restful", description = "Categoria API")
public class CategoriaResource {

    private final CategoriaService service;

    public CategoriaResource(CategoriaService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todas as categorias")
    @ApiResponse(code = 200, message = "Ok", response = Categoria.class)
    //@Cacheable(value = "categorias")
    @GetMapping
    public ResponseEntity<List<Categoria>> getAll() {
        return new ResponseEntity<>(service.getAllCategorias(), HttpStatus.OK);
    }

    @ApiOperation(value = "Obtem categoria por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "categoria", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Categoria> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getCategoriaById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra categoria")
    @ApiResponse(code = 201, message = "Created", response = Categoria.class)
    //@Cacheable(value = "categorias")
   // @CacheEvict(cacheNames = "categorias", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody Categoria categoria) {
        return new ResponseEntity<>(service.addCategoria(categoria), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Atualiza/Cria uma categoria")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "categorias") // @CacheEvict(cacheNames = "categorias", allEntries = true)
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> put(@PathVariable("id") Long id, @RequestBody Categoria categoria) {
        return new ResponseEntity<>(service.updateCategoria(id, categoria), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Remove uma categoria")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"categorias", "categoria"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeById(@PathVariable("id") Long id) {
        service.removeCategoriaById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todas as categorias")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"categorias", "categoria"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAll() {
        service.removeAllCategoria();
        return ResponseEntity.noContent().build();
    }
}
