package br.com.tcc.spring.boot.restful.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.tcc.spring.boot.restful.domain.entity.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Long> {
	
	 @Query("SELECT pedido FROM Pedido pedido where pedido.idComanda = :idComanda")
		    public List<Pedido> findPedidoAgrupado(@Param("idComanda") Long idComanda);
}


