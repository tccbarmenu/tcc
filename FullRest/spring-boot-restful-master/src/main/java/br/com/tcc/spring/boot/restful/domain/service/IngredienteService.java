package br.com.tcc.spring.boot.restful.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.tcc.spring.boot.restful.domain.entity.Ingrediente;
import br.com.tcc.spring.boot.restful.repository.IngredienteRepository;


@Service
public class IngredienteService {

    private final IngredienteRepository repository;

    public IngredienteService(IngredienteRepository repository) {
        this.repository = repository;
    }

    public Ingrediente getIngredienteById(Long id) {
        return repository.findOne(id);
    }

    public List<Ingrediente> getAllIngredientes() {
        return repository.findAll();
    }

    public Long addIngrediente(Ingrediente ingrediente) {
        return repository.save(ingrediente).getId();
    }

    public Long updateIngrediente(Long id, Ingrediente ingrediente) {
    	Ingrediente ingredienteAtualizar = repository.findOne(id);
        ingredienteAtualizar.setNome(ingrediente.getNome());
        ingredienteAtualizar.setDescricao(ingrediente.getDescricao());
        ingredienteAtualizar.setIdEstabelecimento(ingrediente.getIdEstabelecimento());
        return repository.save(ingredienteAtualizar).getId();
    }

    public void removeAllIngrediente() {
        repository.deleteAll();
    }

    public void removeIngredienteById(Long id) {
        repository.delete(id);
    }
}
