package br.com.tcc.spring.boot.restful.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.tcc.spring.boot.restful.domain.entity.StatusPedido;

public interface StatusPedidoRepository extends JpaRepository<StatusPedido, Long> {
	@Query("SELECT idStatusPedido FROM StatusPedido WHERE nome = :nome and idEstabelecimento = :idEstabelecimento")
    public Long getStatusPedidoByEstabelecimento(@Param("nome") String nome, @Param("idEstabelecimento") Long idEstabelecimento);
}
