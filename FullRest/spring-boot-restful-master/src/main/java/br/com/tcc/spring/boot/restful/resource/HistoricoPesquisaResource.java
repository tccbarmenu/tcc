package br.com.tcc.spring.boot.restful.resource;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tcc.spring.boot.restful.domain.entity.HistoricoPesquisa;
import br.com.tcc.spring.boot.restful.domain.service.HistoricoPesquisaService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/historico")
@Api(value = "Bar Menu Restful", description = "Históricos de Pesquisa Usuário API")
public class HistoricoPesquisaResource {

    private final HistoricoPesquisaService service;

    public HistoricoPesquisaResource(HistoricoPesquisaService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todos os historicos")
    @ApiResponse(code = 200, message = "Ok", response = HistoricoPesquisa.class)
    //@Cacheable(value = "historicos")
    @GetMapping
    public ResponseEntity<List<HistoricoPesquisa>> getAll() {
        return new ResponseEntity<>(service.getAllHistoricos(), HttpStatus.OK);
    }

    @ApiOperation(value = "Obtem historico por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "historico", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<HistoricoPesquisa> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getHistoricoById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra historico")
    @ApiResponse(code = 201, message = "Created", response = HistoricoPesquisa.class)
    //@Cacheable(value = "historicos")
   // @CacheEvict(cacheNames = "historicos", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody HistoricoPesquisa historico) {
        return new ResponseEntity<>(service.addHistorico(historico), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Atualiza/Cria um historico")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "historicos")
   // @CacheEvict(cacheNames = "historicos", allEntries = true)
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> put(@PathVariable("id") Long id, @RequestBody HistoricoPesquisa historico) {
        return new ResponseEntity<>(service.updateHistorico(id, historico), HttpStatus.CREATED);
    }

    @ApiOperation(value =  "Remove um historico")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"historicos", "historico"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeById(@PathVariable("id") Long id) {
        service.removeHistoricoById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todos os historicos")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"historicos", "historico"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAll() {
        service.removeAllHistorico();
        return ResponseEntity.noContent().build();
    }
}
