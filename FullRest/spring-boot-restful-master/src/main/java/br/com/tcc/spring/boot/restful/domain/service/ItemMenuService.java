package br.com.tcc.spring.boot.restful.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.tcc.spring.boot.restful.domain.entity.ItemMenu;
import br.com.tcc.spring.boot.restful.repository.ItemMenuRepository;

@Service
public class ItemMenuService {

    private final ItemMenuRepository repository;

    public ItemMenuService(ItemMenuRepository repository) {
        this.repository = repository;
    }

    public ItemMenu getItemMenuById(Long id) {
        return repository.findOne(id);
    }

    public List<ItemMenu> getAllItensMenu() {
        return repository.findAll();
    }
    
    public List<ItemMenu> getAllItemMenuByRestaurante(Long id) {
        return repository.getAllItemMenuByRestaurante(id);
    }

    public Long addItemMenu(ItemMenu itemMenu) {
        return repository.save(itemMenu).getId();
    }

    public Long updateItemMenu(Long id, ItemMenu itemMenu) {
        ItemMenu itemMenuToUpdate = repository.findOne(id);
        itemMenuToUpdate.setTitulo(itemMenu.getTitulo());
        itemMenuToUpdate.setServeQtdPessoas(itemMenu.getServeQtdPessoas());
        itemMenuToUpdate.setPreco(itemMenu.getPreco());
        itemMenuToUpdate.setImagem(itemMenu.getImagem());
        itemMenuToUpdate.setIdCategoria(itemMenu.getIdCategoria());
        itemMenuToUpdate.setDescricao(itemMenu.getDescricao());
        itemMenuToUpdate.setIngredientes(itemMenu.getIngredientes());
        return repository.save(itemMenuToUpdate).getId();
    }

    public void removeAllItensMenu() {
        repository.deleteAll();
    }

    public void removeItemMenuById(Long id) {
        repository.delete(id);
    }
}
