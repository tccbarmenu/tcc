package br.com.tcc.spring.boot.restful.resource;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tcc.spring.boot.restful.domain.entity.Avaliacao;
import br.com.tcc.spring.boot.restful.domain.service.AvaliacaoService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/avaliacao")
@Api(value = "Bar Menu Restful", description = "Categoria API")
public class AvaliacaoResource {

    private final AvaliacaoService service;

    public AvaliacaoResource(AvaliacaoService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todas as avalia��es")
    @ApiResponse(code = 200, message = "Ok", response = Avaliacao.class)
    //@Cacheable(value = "avaliacoes")
    @GetMapping
    public ResponseEntity<List<Avaliacao>> getAll() {
        return new ResponseEntity<>(service.getAllAvaliacoes(), HttpStatus.OK);
    }

    @ApiOperation(value = "Obtem Avalia��o por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "avalicao", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Avaliacao> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getAvaliacaoById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra Avalia��o")
    @ApiResponse(code = 201, message = "Created", response = Avaliacao.class)
    //@Cacheable(value = "avaliacoes")
   // @CacheEvict(cacheNames = "avaliacoes", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody Avaliacao avalicao) {
        return new ResponseEntity<>(service.addAvaliacao(avalicao), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Remove uma avalicao")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"avaliacoes", "avalicao"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeAvaliacaoById(@PathVariable("id") Long id) {
        service.removeAvaliacaoById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todas as avalia��es")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"avaliacoes", "avalicao"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAllAvaliacoes() {
        service.removeAllAvaliacao();
        return ResponseEntity.noContent().build();
    }
}
