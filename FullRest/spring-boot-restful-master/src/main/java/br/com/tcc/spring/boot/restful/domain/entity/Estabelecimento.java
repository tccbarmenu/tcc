package br.com.tcc.spring.boot.restful.domain.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table
public class Estabelecimento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_estabelecimento")
    private Long idEstabelecimento;

    @Valid
    @Column(name="nome_estabelecimento")
    private String nomeEstabelecimento;
    
    @Column(name="descricao_estabelecimento")
    private String descricaoEstabelecimento;
    
    @Column(name="endereco_estabelecimento")
    private String endereco;
    
    @Column(name="cidade_estabelecimento")
    private String cidade;
    
    
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true ,fetch=FetchType.LAZY)
//    @JoinColumn(name = "ID_ESTABELECIMENTO")
//    private List<StatusComanda> statusComanda = new ArrayList<>();
////    
//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "ID_ESTABELECIMENTO")
//    private List<StatusPedido> statusPedido = new ArrayList<>();
    
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name="data_registro")
    private Date dataCadastro;
}
