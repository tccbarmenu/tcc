//package br.com.tcc.spring.boot.restful.resource;
//
//import java.util.List;
//
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import br.com.tcc.spring.boot.restful.domain.entity.IngredienteItemMenu;
//import br.com.tcc.spring.boot.restful.domain.service.IngredienteItemMenuService;
//import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//
//@RestController
//@RequestMapping("/ingredienteItemMenu")
//@Api(value = "Bar Menu Restful", description = "Comanda API")
//public class IngredienteItemMenuResource {
//
//    private final IngredienteItemMenuService service;
//
//    public IngredienteItemMenuResource(IngredienteItemMenuService service) {
//        this.service = service;
//    }
//
//    @ApiOperation(value = "Obtem todas as categorias")
//    @ApiResponse(code = 200, message = "Ok", response = IngredienteItemMenu.class)
//    //@Cacheable(value = "beers")
//    @GetMapping
//    public ResponseEntity<List<IngredienteItemMenu>> getAll() {
//        return new ResponseEntity<>(service.getTodasRelacoes(), HttpStatus.OK);
//    }
//
//    @ApiOperation(value = "Obtem todas as categorias")
//    @ApiResponses(value = {
//            @ApiResponse(code = 201, message = "Ok", response = Number.class),
//            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
//    //@Cacheable(value = "beer", key = "#id")
//    @GetMapping(value = "/{id}")
//    public ResponseEntity<IngredienteItemMenu> getById(@PathVariable("id") Long id) {
//        return new ResponseEntity<>(service.getRelacaoPorIdIngrediente(id), HttpStatus.OK);
//    }
//
//    @ApiOperation(value = "Registra categoria")
//    @ApiResponse(code = 201, message = "Created", response = IngredienteItemMenu.class)
//    //@Cacheable(value = "beers")
//   // @CacheEvict(cacheNames = "beers", allEntries = true)
//    @PostMapping
//    public ResponseEntity<Long> post(@RequestBody IngredienteItemMenu beer) {
//        return new ResponseEntity<>(service.addRelacao(beer), HttpStatus.CREATED);
//    }
//
//    @ApiOperation(value = "Atualiza/Cria uma categoria")
//    @ApiResponses(value = {
//            @ApiResponse(code = 201, message = "Created", response = Number.class),
//            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
//    //@Cacheable(value = "beers")
//   // @CacheEvict(cacheNames = "beers", allEntries = true)
//    @PutMapping(value = "/{id}")
//    public ResponseEntity<Long> put(@PathVariable("id") Long id, @RequestBody IngredienteItemMenu beer) {
//        return new ResponseEntity<>(service.alteraRelacaoPorIdIngrediente(id, beer), HttpStatus.CREATED);
//    }
//
//    @ApiOperation(value =  "Remove uma categoria")
//    @ApiResponses(value = {
//            @ApiResponse(code = 204, message = "Ok", response = Number.class),
//            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
//   // @CacheEvict(cacheNames = {"beers", "beer"}, allEntries = true)
//    @DeleteMapping(value = "/{id}")
//    public ResponseEntity<Void> removeById(@PathVariable("id") Long id) {
//        service.removeRelacaoPorIdIngrediente(id);
//        return ResponseEntity.noContent().build();
//    }
//
//    @ApiOperation(value = "Remove todas as categorias")
//    @ApiResponse(code = 204, message = "Ok", response = Number.class)
//   // @CacheEvict(cacheNames = {"beers", "beer"}, allEntries = true)
//    @DeleteMapping
//    public ResponseEntity<Void> removeAll() {
//        service.removeRelacao();
//        return ResponseEntity.noContent().build();
//    }
//}
