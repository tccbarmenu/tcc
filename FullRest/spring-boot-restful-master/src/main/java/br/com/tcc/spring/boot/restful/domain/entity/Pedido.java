package br.com.tcc.spring.boot.restful.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
@Data
@Entity
@Table
public class Pedido {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID_PEDIDO")
    private Long idPedido;
	
	@Column(name="ID_ESTABELECIMENTO")
    private Long idEstabelecimento;
	
	@Column(name="ID_ITEM_MENU")
    private Long idItemMenu;
	
	@Valid
    @Column(name="ID_COMANDA")
    private Long idComanda;
	
	@Column(name="QUANTIDADE")
    private Long quantidade;
	
	@Valid
    @Column(name="ID_STATUS_PEDIDO")
    private Long idStatusPedido;
	
	@Column(name="OBSERVACAO_PEDIDO")
	private String observacaoPedido;
	
	@Transient
	private String nomePedido;
	
	@Transient
	private Float valorTotalPedidoAgrupado;
	
    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name="data_registro")
    private Date dataCadastro;
}
