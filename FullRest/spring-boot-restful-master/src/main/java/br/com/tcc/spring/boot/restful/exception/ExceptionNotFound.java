package br.com.tcc.spring.boot.restful.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ExceptionNotFound extends RuntimeException {
    public ExceptionNotFound() {
        super("Content not found.");
    }
}
