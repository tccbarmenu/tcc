package br.com.tcc.spring.boot.restful.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.tcc.spring.boot.restful.domain.entity.Recomendacao;

public interface RecomendacaoRepository extends JpaRepository<Recomendacao, Long> {
	 @Query("SELECT rec FROM Recomendacao rec WHERE rec.idUsuario = :idUsuario and rec.idEstabelecimento = :idEstabelecimento "
	 		+ "AND rec.nota = (SELECT MAX(nota) FROM Recomendacao REC2 WHERE REC2.idUsuario = rec.idUsuario and REC2.idEstabelecimento =rec.idEstabelecimento)")
	    public Recomendacao findRecomendacaoToUserByIdEstabelecimento(@Param("idUsuario") Long idUsuario, @Param("idEstabelecimento") Long idEstabelecimento);
	 
	 @Query("SELECT idUsuario FROM Usuario")
	    public List<Long> getAllIdUsuario();
	 
	 @Query("SELECT idEstabelecimento FROM ItemMenu item where item.id = :idItem")
	    public Long getIdEstabelecimento(@Param("idItem") Long idItem);
}
