package br.com.tcc.spring.boot.restful.resource;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.tcc.spring.boot.restful.domain.entity.Recomendacao;
import br.com.tcc.spring.boot.restful.domain.entity.StatusComanda;
import br.com.tcc.spring.boot.restful.domain.service.StatusComandaService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/statusComanda")
@Api(value = "Bar Menu Restful", description = "Status Comanda API")
public class StatusComandaResource {

    private final StatusComandaService service;

    public StatusComandaResource(StatusComandaService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todos os status das comandas")
    @ApiResponse(code = 200, message = "Ok", response = StatusComanda.class)
    //@Cacheable(value = "statusComandas")
    @GetMapping
    public ResponseEntity<List<StatusComanda>> getAll() {
        return new ResponseEntity<>(service.getAllStatusComanda(), HttpStatus.OK);
    }

    @ApiOperation(value = "Obtem status da comanda por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "statusComanda", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<StatusComanda> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getStatusComandaById(id), HttpStatus.OK);
    }
    
    @ApiOperation(value = "Obtem status da comanda por Estabelecimento")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "recomendacao", key = "#id")
    @GetMapping(value = "byEstabelecimento")
    public ResponseEntity<Long> getToUsuarioByEstabelecimento(@RequestParam ("nome") String nome, @RequestParam ("idEstabelecimento") Long idEstabelecimento) {
        return new ResponseEntity<>(service.getStatusByEstabelecimento(nome, idEstabelecimento), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra status da comanda")
    @ApiResponse(code = 201, message = "Created", response = StatusComanda.class)
    //@Cacheable(value = "statusComandas")
   // @CacheEvict(cacheNames = "statusComandas", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody StatusComanda statusComanda) {
        return new ResponseEntity<>(service.addStatusComada(statusComanda), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Atualiza/Cria o status da comanda")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "statusComandas")
   // @CacheEvict(cacheNames = "statusComandas", allEntries = true)
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> put(@PathVariable("id") Long id, @RequestBody StatusComanda statusComanda) {
        return new ResponseEntity<>(service.updateStatusComanda(id, statusComanda), HttpStatus.CREATED);
    }

    @ApiOperation(value =  "Remove o status da comanda")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"statusComandas", "statusComanda"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeById(@PathVariable("id") Long id) {
        service.removeStatusComandaById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todas os status das comandas")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"statusComandas", "statusComanda"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAll() {
        service.removeAllStatusComanda();
        return ResponseEntity.noContent().build();
    }
}
