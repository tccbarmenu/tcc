package br.com.tcc.spring.boot.restful.resource;

import br.com.tcc.spring.boot.restful.domain.entity.Estabelecimento;
import br.com.tcc.spring.boot.restful.domain.service.EstabelecimentoService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estabelecimento")
@Api(value = "Bar Menu Restful", description = "Comanda API")
public class EstabelecimentoResource {
	private final EstabelecimentoService service;

	public EstabelecimentoResource (EstabelecimentoService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todas os estabelecimento")
    @ApiResponse(code = 200, message = "Ok", response = Estabelecimento.class)
    //@Cacheable(value = "estabelecimentos")
    @GetMapping
    public ResponseEntity<List<Estabelecimento>> getAll() {
        return new ResponseEntity<>(service.getAllEstabelecimentos(), HttpStatus.OK);
    }

    @ApiOperation(value = "Obtem estabelecimento por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "estabelecimento", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Estabelecimento> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getEstabelecimentoById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra estabelecimento")
    @ApiResponse(code = 201, message = "Created", response = Estabelecimento.class)
    //@Cacheable(value = "estabelecimentos")
   // @CacheEvict(cacheNames = "estabelecimentos", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody Estabelecimento estabelecimento) {
        return new ResponseEntity<>(service.addEstabelecimento(estabelecimento), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Atualiza/Cria um estabelecimento")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "estabelecimentos")
   // @CacheEvict(cacheNames = "estabelecimentos", allEntries = true)
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> put(@PathVariable("id") Long id, @RequestBody Estabelecimento estabelecimento) {
        return new ResponseEntity<>(service.updateEstabelecimento(id, estabelecimento), HttpStatus.CREATED);
    }

    @ApiOperation(value =  "Remove um estabelecimento")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"estabelecimentos", "estabelecimento"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeById(@PathVariable("id") Long id) {
        service.removeEstabelecimentoById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todos os estabelecimento")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"estabelecimentos", "estabelecimento"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAll() {
        service.removeAllEstabelecimentos();
        return ResponseEntity.noContent().build();
    }
}
