package br.com.tcc.spring.boot.restful.resource;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tcc.spring.boot.restful.domain.entity.Ingrediente;
import br.com.tcc.spring.boot.restful.domain.service.IngredienteService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/ingrediente")
@Api(value = "Bar Menu Restful", description = "Ingrediente API")
public class IngredienteResource {

    private final IngredienteService service;

    public IngredienteResource(IngredienteService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todos os ingredientes")
    @ApiResponse(code = 200, message = "Ok", response = Ingrediente.class)
    //@Cacheable(value = "ingredientes")
    @GetMapping
    public ResponseEntity<List<Ingrediente>> getAll() {
        return new ResponseEntity<>(service.getAllIngredientes(), HttpStatus.OK);
    }

    @ApiOperation(value = "Obtem ingrediente por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "ingrediente", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Ingrediente> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getIngredienteById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra ingrediente")
    @ApiResponse(code = 201, message = "Created", response = Ingrediente.class)
    //@Cacheable(value = "ingredientes")
   // @CacheEvict(cacheNames = "ingredientes", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody Ingrediente ingrediente) {
        return new ResponseEntity<>(service.addIngrediente(ingrediente), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Atualiza/Cria um ingrediente")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "ingredientes")
   // @CacheEvict(cacheNames = "ingredientes", allEntries = true)
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> put(@PathVariable("id") Long id, @RequestBody Ingrediente ingrediente) {
        return new ResponseEntity<>(service.updateIngrediente(id, ingrediente), HttpStatus.CREATED);
    }

    @ApiOperation(value =  "Remove um ingrediente")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"ingredientes", "ingrediente"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeById(@PathVariable("id") Long id) {
        service.removeIngredienteById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todos os ingredientes")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"ingredientes", "ingrediente"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAll() {
        service.removeAllIngrediente();
        return ResponseEntity.noContent().build();
    }
}
