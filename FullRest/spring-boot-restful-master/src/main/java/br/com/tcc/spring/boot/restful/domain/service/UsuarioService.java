package br.com.tcc.spring.boot.restful.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.tcc.spring.boot.restful.domain.entity.Usuario;
import br.com.tcc.spring.boot.restful.repository.UsuarioRepository;

@Service
public class UsuarioService {

    private final UsuarioRepository repository;

    public UsuarioService(UsuarioRepository repository) {
        this.repository = repository;
    }

    public Usuario getUsuarioById(Long id) {
        return repository.findOne(id);
    }

    public Long getUsuarioByIdUsuarioFirebase(String idUsuarioFirebase) {
        return repository.getUsuarioByIdUsuarioFirebase(idUsuarioFirebase);
    }
    
    public List<Usuario> getAllUsuarios() {
        return repository.findAll();
    }

    public Long addUsuarios(Usuario user) {
        return repository.save(user).getIdUsuario();
    }

    public Long updateUsuario(Long id, Usuario user) {
    	Usuario usuarioAtualizar = repository.findOne(id);
    	usuarioAtualizar.setPerguntaUm(user.getPerguntaUm());
    	usuarioAtualizar.setPerguntaDois(user.getPerguntaDois());
    	usuarioAtualizar.setPerguntaTres(user.getPerguntaTres());
//        usuarioAtualizar.setNome(user.getNome());
//        usuarioAtualizar.setIdUsuarioFirebase(user.getIdUsuarioFirebase());
//        usuarioAtualizar.setNiveAcesso(user.getNiveAcesso());
//        usuarioAtualizar.setCidade(user.getCidade());
//        usuarioAtualizar.setDataNascimento(user.getDataNascimento());
//        usuarioAtualizar.setSexo(user.getSexo());
        return repository.save(usuarioAtualizar).getIdUsuario();
    }

    public void removeAllUsuarios() {
        repository.deleteAll();
    }

    public void removeUsuarioById(Long id) {
        repository.delete(id);
    }
}
