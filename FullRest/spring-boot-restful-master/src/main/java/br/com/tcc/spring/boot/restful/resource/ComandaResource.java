package br.com.tcc.spring.boot.restful.resource;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.tcc.spring.boot.restful.domain.entity.Comanda;
import br.com.tcc.spring.boot.restful.domain.service.ComandaService;
import br.com.tcc.spring.boot.restful.exception.ExceptionNotFound;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/comanda")
@Api(value = "Bar Menu Restful", description = "Comanda API")
public class ComandaResource {

    private final ComandaService service;

    public ComandaResource(ComandaService service) {
        this.service = service;
    }

    @ApiOperation(value = "Obtem todas as comandas")
    @ApiResponse(code = 200, message = "Ok", response = Comanda.class)
    //@Cacheable(value = "comandas")
    @GetMapping
    public ResponseEntity<List<Comanda>> getAll() {
        return new ResponseEntity<>(service.getAllComandas(), HttpStatus.OK);
    }

    @ApiOperation(value = "Obtem comanda por Id")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "comanda", key = "#id")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Comanda> getById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(service.getComandaById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Registra comanda")
    @ApiResponse(code = 201, message = "Created", response = Comanda.class)
    //@Cacheable(value = "comandas")
   // @CacheEvict(cacheNames = "comandas", allEntries = true)
    @PostMapping
    public ResponseEntity<Long> post(@RequestBody Comanda comanda) {
        return new ResponseEntity<>(service.addComanda(comanda), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Atualiza Valor Total")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "comandas")
   // @CacheEvict(cacheNames = "comandas", allEntries = true)
    @PutMapping(value = "AtualizaValorTotal")
    public ResponseEntity<Long> put(@RequestParam ("idComanda") Long idComanda, @RequestBody Comanda comanda) {
        return new ResponseEntity<>(service.updateValorTotalComanda(idComanda, comanda), HttpStatus.CREATED);
    }
    
    @ApiOperation(value = "Atualiza/Cria uma comanda")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
    //@Cacheable(value = "comandas")
   // @CacheEvict(cacheNames = "comandas", allEntries = true)
    @PutMapping(value = "/{id}")
    public ResponseEntity<Long> putValorTotal(@PathVariable("id") Long id, @RequestBody Comanda comanda) {
        return new ResponseEntity<>(service.updateComanda(id, comanda), HttpStatus.CREATED);
    }

    @ApiOperation(value =  "Remove uma comanda")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Ok", response = Number.class),
            @ApiResponse(code = 401, message = "Not Found", response = ExceptionNotFound.class)})
   // @CacheEvict(cacheNames = {"comandas", "comanda"}, allEntries = true)
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> removeById(@PathVariable("id") Long id) {
        service.removeComandaById(id);
        return ResponseEntity.noContent().build();
    }

    @ApiOperation(value = "Remove todas as comanda")
    @ApiResponse(code = 204, message = "Ok", response = Number.class)
   // @CacheEvict(cacheNames = {"comandas", "comanda"}, allEntries = true)
    @DeleteMapping
    public ResponseEntity<Void> removeAll() {
        service.removeAllComanda();
        return ResponseEntity.noContent().build();
    }
}
