package br.com.tcc.spring.boot.restful.domain.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.tcc.spring.boot.restful.domain.entity.StatusComanda;
import br.com.tcc.spring.boot.restful.repository.StatusComandaRepository;



@Service
public class StatusComandaService {

    private final StatusComandaRepository repository;

    public StatusComandaService(StatusComandaRepository repository) {
        this.repository = repository;
    }

    public StatusComanda getStatusComandaById(Long id) {
        return repository.findOne(id);
    }

    public List<StatusComanda> getAllStatusComanda() {
        return repository.findAll();
    }

    public Long addStatusComada(StatusComanda status) {
        return repository.save(status).getIdStatusComanda();
    }
    
    public Long getStatusByEstabelecimento(String nome, Long idEstabelecimento) {
        return repository.getStatusComandaByEstabelecimento(nome, idEstabelecimento);
    }

    public Long updateStatusComanda(Long id, StatusComanda status) {
    	StatusComanda statusAtualizar = repository.findOne(id);
        statusAtualizar.setNome(status.getNome());
        statusAtualizar.setDescricao(status.getDescricao());
        statusAtualizar.setIdEstabelecimento(status.getIdEstabelecimento());
        return repository.save(statusAtualizar).getIdStatusComanda();
    }

    public void removeAllStatusComanda() {
        repository.deleteAll();
    }

    public void removeStatusComandaById(Long id) {
        repository.delete(id);
    }
}
