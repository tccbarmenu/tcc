package br.com.tcc.spring.boot.restful.domain.entity;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
public class HistoricoPesquisa {
    // preenchidos automaticamente 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID_HISTORICO_PESQUISA")
    private Long idHistoricoPesquisa;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @Column(name="data_registro")
    private Date dataCadastro;
    
    /// possui Anotattion
    @Column(name="NOME_PRATO")
    private String nomePrato;
    
    @Column(name="ID_USUARIO")
    private String idUsuario;
    
    @Column(name="VALOR_PRATO")
    @Pattern(regexp="([0-9]{1,3}\\.)?[0-9]{1,3},[0-9]{2}$")
    private String valorPrato;
    
    @Column(name="FOI_PEDIDO")
    private Boolean foiPedido;
    
    @Column(name="INGREDIENTES_REMOVIDOS")
    HashMap<Long, String> ingredientesRemovidos;
    
    @Column(name="INGREDIENTES_MANTIDOS")
    HashMap<Long, String> ingredientesMantidos;
    
    /// n�o possui anota��o
    @Column(name="categoria")
    private String categoria;
    
    
    
    

}
