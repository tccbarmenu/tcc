package br.com.tcc.spring.boot.restful.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.tcc.spring.boot.restful.domain.entity.StatusComanda;

public interface StatusComandaRepository extends JpaRepository<StatusComanda, Long> {
	@Query("SELECT idStatusComanda FROM StatusComanda WHERE nome = :nome and idEstabelecimento = :idEstabelecimento")
	    public Long getStatusComandaByEstabelecimento(@Param("nome") String nome, @Param("idEstabelecimento") Long idEstabelecimento);
}
